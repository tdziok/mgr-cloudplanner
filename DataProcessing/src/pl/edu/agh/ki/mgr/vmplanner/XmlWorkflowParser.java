package pl.edu.agh.ki.mgr.vmplanner;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

public class XmlWorkflowParser {

  private static final String namespaceURI = null;

  // private static final String namespaceURI =
  // "http://pegasus.isi.edu/schema/DAX";

  public static void main(String[] args) throws Exception {
    new XmlWorkflowParser().parseXmlWorkflow("resources\\workflow.xml");

  }

  XmlWorkflow parseXmlWorkflow(String string) throws Exception {
    List<Job> jobs = new ArrayList<XmlWorkflowParser.Job>();
    List<Child> children = new ArrayList<XmlWorkflowParser.Child>();
    XMLInputFactory factory = XMLInputFactory.newFactory();
    XMLStreamReader xmlStreamReader = factory
        .createXMLStreamReader(new BufferedInputStream(new FileInputStream(
            string)));

    Child currentChild = new Child();
    while (xmlStreamReader.hasNext()) {
      int event = xmlStreamReader.next();

      switch (event) {
      case XMLStreamConstants.START_ELEMENT: {
        String localName = xmlStreamReader.getLocalName();
        if ("job".equals(localName)) {
          Job job = new Job();

          String id = xmlStreamReader.getAttributeValue(namespaceURI, "id");
          String name = xmlStreamReader.getAttributeValue(namespaceURI, "name");
          job.setId(id);
          job.setName(name);
          jobs.add(job);
        } else if ("child".equals(localName)) {
          currentChild = new Child();
          currentChild.setRef(xmlStreamReader.getAttributeValue(namespaceURI,
              "ref"));
          children.add(currentChild);
        } else if ("parent".equals(localName)) {
          currentChild.addParent(xmlStreamReader.getAttributeValue(
              namespaceURI, "ref"));
        }

        break;
      }
      }
    }

    System.out.println(jobs);
    System.out.println(children);

    return new XmlWorkflow(jobs, children);
  }

  static class XmlWorkflow {
    List<Job> jobs;
    List<Child> children;

    public List<Job> getJobs() {
      return jobs;
    }

    public List<Child> getChildren() {
      return children;
    }

    public XmlWorkflow(List<Job> jobs, List<Child> children) {
      super();
      this.jobs = jobs;
      this.children = children;
    }

  }

  static class Job {
    private String id;
    private String name;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return "Job [id=" + id + ", name=" + name + "]";
    }

  }

  static class Child {
    String ref;
    List<String> parents = new ArrayList<String>();

    public List<String> getParents() {
      return parents;
    }

    public void addParent(String parent) {
      this.parents.add(parent);
    }

    public void setRef(String ref) {
      this.ref = ref;
    }

    public String getRef() {
      return ref;
    }

    @Override
    public String toString() {
      return "Child [ref=" + ref + ", parents=" + parents + "]";
    }

  }

}
