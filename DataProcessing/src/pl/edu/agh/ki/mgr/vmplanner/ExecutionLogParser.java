package pl.edu.agh.ki.mgr.vmplanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExecutionLogParser {
  public static void main(String[] args) {
    // new ExecutionLogParser()
    // .parseAwsLog("D:\\studia\\mgr\\dane z aws\\logs\\m3.large.5.0.log");
    // new ExecutionLogParser().parseAwsLog("resources\\simple.log");

  }

  public Map<String, List<LogEntry>> parseAwsLog(String string) {
    try (BufferedReader reader = new BufferedReader(new FileReader(string))) {
      Map<String, List<LogEntry>> entries = reader
          .lines()
          //
          .map(line -> {
            String[] split = line.split("\\s");
            LogEntry logEntry = new LogEntry();
            if (split.length > 2) {
              logEntry.instance = split[0];
              logEntry.task = split[5];
              logEntry.execution = split[6];
            }
            return logEntry;
          })
          .filter(logEntry -> logEntry.instance != null)
          .collect(
              Collectors.groupingBy(LogEntry::getTask, Collectors.toList()));

      return entries;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  static class LogEntry {
    private String instance;
    private String task;
    private String execution;

    @Override
    public String toString() {
      return "LogEntry [instance=" + instance + ", task=" + task
          + ", execution=" + execution + "]";
    }

    public String getTask() {
      return task;
    }

    public String getExecution() {
      return execution;
    }

  }

}
