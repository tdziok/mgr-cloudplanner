package pl.edu.agh.ki.mgr.vmplanner;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.ki.mgr.vmplanner.ExecutionLogParser.LogEntry;
import pl.edu.agh.ki.mgr.vmplanner.XmlWorkflowParser.Job;
import pl.edu.agh.ki.mgr.vmplanner.XmlWorkflowParser.XmlWorkflow;

public class XmlAndLogToDagTranslator {

  ExecutionLogParser executionLogParser = new ExecutionLogParser();
  XmlWorkflowParser xmlWorkflowParser = new XmlWorkflowParser();

  public static void main(String[] args) throws Exception {
    // String log = "resources\\simple.log";
    String log = "D:\\studia\\mgr\\dane z aws\\logs\\m3.large.5.0.log";
    // String xml = "resources\\workflow.xml";
    String xml = "D:\\studia\\mgr\\dane z aws\\montage.5.0.xml";
    new XmlAndLogToDagTranslator().mergeXmlAndLogToDAG(log, xml);
  }

  private void mergeXmlAndLogToDAG(String logFile, String xmlFile)
      throws Exception {
    final Map<String, List<LogEntry>> awsLog = executionLogParser
        .parseAwsLog(logFile);
    final Map<String, Integer> lastUsedTimeIndex = new HashMap<>();
    XmlWorkflow xmlWorkflow = xmlWorkflowParser.parseXmlWorkflow(xmlFile);

    try (BufferedWriter writer = new BufferedWriter(
        new FileWriter("output.dag"))) {

      xmlWorkflow.getJobs()
          .stream()
          //
          .forEach(
              job -> {
                try {
                  writer.write(String.format("TASK %s %s %s", job.getId(),
                      job.getName(),
                      getTimeForTask(job, awsLog, lastUsedTimeIndex)));
                  writer.newLine();
                } catch (Exception e) {
                  e.printStackTrace();
                }
              });

      writer.newLine();

      xmlWorkflow.getChildren()
          .stream()
          //
          .forEach(
              child -> {
                child
                    .getParents()
                    .stream()
                    .forEach(
                        parent -> {
                          try {
                            writer.write(String.format("EDGE %s %s",
                                child.getRef(), parent));
                            writer.newLine();
                          } catch (Exception e) {
                            e.printStackTrace();
                          }
                        });
              });
      writer.newLine();
      writer.flush();
    }

  }

  private Object getTimeForTask(Job job, Map<String, List<LogEntry>> awsLog,
      Map<String, Integer> lastUsedTimeIndex) {

    String name = job.getName();
    List<LogEntry> timesList = awsLog.get(name);
    Integer currentIndex = 0;
    if (lastUsedTimeIndex.containsKey(name)) {
      currentIndex = (lastUsedTimeIndex.get(name) + 1) % timesList.size();
    }
    lastUsedTimeIndex.put(name, currentIndex);

    return timesList.get(currentIndex).getExecution();
  }
}
