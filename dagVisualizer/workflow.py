class ComparableByAttributes(object):
  def __str__(self):
    return str(self.__dict__)

  def __eq__(self, other):
    return self.__dict__ == other.__dict__


class Task(ComparableByAttributes):
  def __init__(self, id, makespan, type=''):
    self.id = id
    self.makespan = makespan
    self.type = type

    self.after = []
    self.before = []

    self.files_needed = []
    self.files_produced = []

  def __str__(self):
    #return str(self.__dict__)
    return "TASK: id: {}, makespan: {}, #input: {}, #output: {}, #before: {}, after: {}".format(self.id, self.makespan, len(self.files_needed), len(self.files_produced), len(self.before), len(self.after))

class File(ComparableByAttributes):
  def __init__(self, filename, size):
    self.filename = filename
    self.size = size

  def __str__(self):
    #return str(self.__dict__)
    return "FILE: {}, size: {}".format(self.filename, self.size)


class Dag(object):
  def __init__(self, tasks, files):
    self.tasks = tasks
    self.files = files
    self.id = None

  def __str__(self):
    response = "DAG: #Tasks {}, #Files {}\n".format(len(self.tasks), len(self.files))
    response += "\nTASKS:\n"
    for task in self.tasks:
      response += task.__str__() + "\n"

    response += "\nFILES:\n"
    for file in self.files:
      response += file.__str__() + "\n"
    return response

class Step(ComparableByAttributes):
  def __init__(self, stepNbr, task, vm):
    self.stepNbr = stepNbr
    self.task = task
    self.vm = vm
    self.executed = False

  def __str__(self):
    return "STEP: {} {} {}".format(self.stepNbr, self.task.id, self.vm.name)


class Vm(ComparableByAttributes):
  def __init__(self, name):
    self.name = name

  def __str__(self):
    return self.name

class IAAS(object):
  def __init__(self, dag, steps, vms):
    self.dag = dag
    self.steps = steps
    self.vms = vms

  def __str__(self):
    response = "-----------------------\nIAAS:\n"
    response += str(self.dag)
    response += "\nSTEPS: #steps: {}\n".format(len(self.steps))
    for step in self.steps:
      response += str(step) + "\n"
    response += "\nVMs: #vms: {}\n".format(len(self.vms))
    for vm in self.vms:
      response += str(vm) + "\n"

    return response +  "-----------------------\n"

class DagBuilder(object):
  def __init__(self):
    self.tasks = {}
    self.files = []
    self.steps = []
    self.vms = {}

  def add_task(self, task):
    self.tasks[task.id] = task

  def add_edge(self, from_id, to_id):
    from_task = self.tasks[from_id]
    to_task = self.tasks[to_id]

    from_task.after.append(to_task)
    to_task.before.append(from_task)

  def add_file(self, file):
    self.files.append(file)

  def add_input_file(self, task_id, file_id):
    self.tasks[task_id].files_needed.append(file_id)

  def add_output_file(self, task_id, file_id):
    self.tasks[task_id].files_produced.append(file_id)

  def add_vm(self, vm_name):
    self.vms[vm_name] = Vm(vm_name)

  def add_step(self, stepNbr, task_id, vm_name):
    task = self.tasks[task_id]
    vm = self.vms[vm_name]

    self.steps.append(Step(stepNbr, task, vm))

  def build(self):
    return IAAS(Dag(self.tasks.values(), self.files), self.steps, self.vms)
