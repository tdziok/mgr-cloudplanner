from workflow import *

def log(msg):
  print(msg)

def visualize(iaas):
  print(iaas)
  print("IAAS EXECUTION:")

  executedStepsCount = 0

  startedTasks = {}
  vmPlan = {}
  vmAvail = {}

  for vm in iaas.vms:
    vmAvail[vm] = 0
    vmPlan[vm] = []

  time = 0
  maxTime = 0
  while executedStepsCount < len(iaas.steps):
    for step in iaas.steps:
      if step.executed:
        continue

      task = step.task
      vm = step.vm

      areAllBeforeTasksExecuted = True
      for beforeTask in task.before:
        if not beforeTask.id in startedTasks:
          areAllBeforeTasksExecuted = False
          break

        if startedTasks[beforeTask.id] >= time:
          areAllBeforeTasksExecuted = False
          break

      if not areAllBeforeTasksExecuted:
        continue

      isCurrentVmAvail = vmAvail[vm.name] <= time
      if not isCurrentVmAvail:
        continue

      timeForTask = task.makespan
      endTime = time + timeForTask
      vmAvail[vm.name] = endTime
      vmPlan[vm.name].append((time, endTime, task.id))
      startedTasks[task.id] = endTime
      step.executed = True
      executedStepsCount += 1

      if maxTime < endTime:
        maxTime = endTime

      log("[STARTED] task: {}, VM: {}, currentTime: {},  endTime: {}, stepId: {}".format(task.id, vm.name, time, endTime, step.stepNbr))

    time +=1

  print("======================")
  print("DONE:")
  print("(StartTime, EndTime, TaskId)")
  for vm in sorted(vmPlan):
    print("   {}:".format(vm))
    print(vmPlan[vm])
  print("======================")

  for vm in sorted(vmPlan):
    for t in range(0, int(maxTime)):
      task = None
      for (time, endTime, task_id) in vmPlan[vm]:
        if t >= time and t <= endTime:
          task = task_id

      if task:
        print(task[-1:],end="" )
      else:
        print(" ",end="" )

    print()
