import re
import workflow


FILE_PATTERN = r'FILE\s+(\w|.+)\s+(\d+)'
TASK_PATTERN = r'TASK\s+(\w+)\s+(\S+)\s+(\w+)'
EDGE_PATTERN = r'EDGE\s+(\w+)\s+(\w+)'
INPUTS_PATTERN = r'INPUTS\s+(\w+)\s+(.+)'
OUTPUTS_PATTERN = r'OUTPUTS\s+(\w+)\s+(.+)'
VM_PATTERN = r'VM\s+(\w+)'
PLAN_PATTERN = r'PLAN\s+(\d+)\s+(\w+)\s+(\w+)'

def parse_file_line(line):
  match = re.match(FILE_PATTERN, line)
  if not match:
    return None

  filename = match.group(1)
  size = int(match.group(2))
  return workflow.File(filename, size)


def parse_task_line(line):
  match = re.match(TASK_PATTERN, line)
  if not match:
    return None

  id = match.group(1)
  type = match.group(2)
  makespan = float(match.group(3))
  return workflow.Task(id, makespan, type)


def parse_edge_line(line):
  match = re.match(EDGE_PATTERN, line)
  if not match:
    return None

  before_id = match.group(1)
  after_id = match.group(2)
  return before_id, after_id


def parse_inputs_line(line):
  match = re.match(INPUTS_PATTERN, line)
  if not match:
    return None

  task_id = match.group(1)
  filenames = match.group(2)
  return task_id, filenames.split()


def parse_outputs_line(line):
  match = re.match(OUTPUTS_PATTERN, line)
  if not match:
    return None

  task_id = match.group(1)
  filenames = match.group(2)
  return task_id, filenames.split()

def parse_vm(line):
  match = re.match(VM_PATTERN, line)
  if not match:
    return None

  name = match.group(1)
  return name

def parse_plan(line):
  match = re.match(PLAN_PATTERN, line)
  if not match:
    return None

  orderNbr = int(match.group(1))
  task_id = match.group(2)
  vm_name = match.group(3)

  return orderNbr, task_id, vm_name


def parse_dag(dag_file_content):
  dag_builder = workflow.DagBuilder()

  for line in dag_file_content.splitlines():
    dag_file = parse_file_line(line)

    if dag_file:
      dag_builder.add_file(dag_file)
      continue

    dag_task = parse_task_line(line)

    if dag_task:
      dag_builder.add_task(dag_task)
      continue

    dag_edge = parse_edge_line(line)

    if dag_edge:
      before_id, after_id = dag_edge
      dag_builder.add_edge(before_id, after_id)
      continue

    dag_inputs = parse_inputs_line(line)

    if dag_inputs:
      task_id, filenames = dag_inputs
      for filename in filenames:
        dag_builder.add_input_file(task_id, filename)

      continue

    dag_outputs = parse_outputs_line(line)

    if dag_outputs:
      task_id, filenames = dag_outputs
      for filename in filenames:
        dag_builder.add_output_file(task_id, filename)

      continue

    vm_output = parse_vm(line)
    if vm_output:
      dag_builder.add_vm(vm_output)
      continue

    plan_output = parse_plan(line)
    if plan_output:
      stepNbr, task_id, vm_name = plan_output
      dag_builder.add_step(stepNbr, task_id, vm_name)


  return dag_builder.build()
