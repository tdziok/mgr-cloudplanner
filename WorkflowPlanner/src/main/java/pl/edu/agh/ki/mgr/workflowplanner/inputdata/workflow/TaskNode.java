package pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow;

import java.util.HashSet;
import java.util.Set;

public class TaskNode {

  private long longId;
  private String stringId;
  private String name;
  private double estimatedCcu;

  private Set<TaskNode> parents = new HashSet<TaskNode>();
  private Set<TaskNode> children = new HashSet<TaskNode>();

  public TaskNode(long longId, String stringId, String name, double estimatedCcu) {
    this.longId = longId;
    this.stringId = stringId;
    this.name = name;
    this.estimatedCcu = estimatedCcu;
  }

  public long getLongId() {
    return longId;
  }

  public String getStringId() {
    return stringId;
  }

  public String getName() {
    return name;
  }

  public double getEstimatedCcu() {
    return estimatedCcu;
  }

  public void addParent(TaskNode parent) {
    parents.add(parent);
  }

  public void removeParent(TaskNode parent) {
    parents.remove(parent);
  }

  public boolean hasParents() {
    return !parents.isEmpty();
  }

  public void addChild(TaskNode child) {
    children.add(child);
  }

  public Set<TaskNode> getChildren() {
    return children;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((stringId == null) ? 0 : stringId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    TaskNode other = (TaskNode) obj;
    if (stringId == null) {
      if (other.stringId != null) return false;
    } else if (!stringId.equals(other.stringId)) return false;
    return true;
  }

}
