package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplConstants;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel;

public class GlobalPlannerWithDeadline extends WorkflowGlobalPhasePlanner {
  public GlobalPlannerWithDeadline() {
    super(CmplConstants.CMLP_WORKFLOW_MODEL_MIN_COST_PATH, GlobalPlanningModel.MAIN_MINIMIZE_COST_UNDER_DEADLINE);
  }

}
