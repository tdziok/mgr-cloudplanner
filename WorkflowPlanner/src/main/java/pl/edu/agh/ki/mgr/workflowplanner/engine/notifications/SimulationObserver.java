package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public interface SimulationObserver {

  void onProgressUpdate(SimulationProgressNotification notification);

}
