package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import java.util.ArrayList;
import java.util.List;

public class LevelAssignmentResultsToDisplay {

  private long levelId;
  private long estimatedTime;
  private long estimatedCost;

  private List<LevelDetailedResultsToDisplay> levels = new ArrayList<>();

  public void setLevelId(long levelId) {
    this.levelId = levelId;
  }

  public long getLevelId() {
    return levelId;
  }

  public List<LevelDetailedResultsToDisplay> getLevels() {
    return levels;
  }

  public void setLevels(List<LevelDetailedResultsToDisplay> levels) {
    this.levels = levels;
  }

  public long getEstimatedCost() {
    return estimatedCost;
  }

  public void setEstimatedCost(long estimatedCost) {
    this.estimatedCost = estimatedCost;
  }

  public long getEstimatedTime() {
    return estimatedTime;
  }

  public void setEstimatedTime(long estimatedTime) {
    this.estimatedTime = estimatedTime;
  }

}
