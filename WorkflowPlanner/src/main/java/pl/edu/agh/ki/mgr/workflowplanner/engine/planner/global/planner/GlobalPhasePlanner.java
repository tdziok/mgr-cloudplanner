package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;

public interface GlobalPhasePlanner {

  WorkflowAssignmentResult planWorkflow(WorkflowGlobalPlannerInputModel inputModel, String pathToSolution);

}
