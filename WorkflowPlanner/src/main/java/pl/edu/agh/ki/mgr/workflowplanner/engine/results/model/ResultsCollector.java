package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import java.util.ArrayList;
import java.util.List;

public class ResultsCollector {

  private List<IterationResults> results = new ArrayList<>();

  private long givenDeadline;

  public List<IterationResults> getResults() {
    return results;
  }

  public void addResult(IterationResults iterationResults) {
    results.add(iterationResults);
  }

  public long getElapsedRealTime() {
    return results.stream().map(result -> result.getLevelExecutionResultsToDisplay().getExecutionTime()).reduce((l1, l2) -> l1 + l2).get();
  }

  public long getCurrentRealCost() {
    return results.stream().map(result -> result.getLevelExecutionResultsToDisplay().getExecutionCost()).reduce((l1, l2) -> l1 + l2).get();
  }

  public long getGivenDeadline() {
    return givenDeadline;
  }

  public void setGivenDeadline(long givenDeadline) {
    this.givenDeadline = givenDeadline;
  }

  public long getMaxYValueForTime() {
    long maxTimeValue = givenDeadline;
    long totalRealExecutionTime = 0;

    for (IterationResults r : results) {

      long totalEstimatedTimeFromGlobalPlanning = r.getWorkflowAssignmentResultsToDisplay().getTotalEstimatedTime();
      if (totalEstimatedTimeFromGlobalPlanning > maxTimeValue) {
        maxTimeValue = totalEstimatedTimeFromGlobalPlanning;
      }

      long estimatedTimeFromNextLevel = r.getLevelAssignmentResultsToDisplay().getEstimatedTime();
      if (totalRealExecutionTime + estimatedTimeFromNextLevel > maxTimeValue) {
        maxTimeValue = totalRealExecutionTime + estimatedTimeFromNextLevel;
      }

      final Long realExecutionTimeForNextLevel = r.getLevelExecutionResultsToDisplay().getExecutionTime();
      if (totalRealExecutionTime + realExecutionTimeForNextLevel > maxTimeValue) {
        maxTimeValue = totalRealExecutionTime + realExecutionTimeForNextLevel;
      }

      totalRealExecutionTime += realExecutionTimeForNextLevel;
    }

    return maxTimeValue;
  }
}
