package pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CloudManagerModelConverter {

  WorkflowGlobalPlannerInputModel toWorkflowGlobalPlannerInput(InputDataModel inputModel) {
    Long globalDeadline = inputModel.getGlobalDeadline();
    List<Level> levelInfoList = new ArrayList<>(inputModel.getLevels());
    List<Vm> availableVms = new ArrayList<>(inputModel.getVms());

    return new WorkflowGlobalPlannerInputModel(globalDeadline, levelInfoList, availableVms);
  }

  LocalPlannerInputModel toWorkflowLocalPlannerInput(Level level, WorkflowAssignmentResult planWorkflowResults) {
    Map<Vm, Long> assignedVms = new HashMap<>();
    List<WorkflowAssignmentResult.VmWrapper> vmWrappers = planWorkflowResults.getLevelToVmsMap().get(level);

    vmWrappers.stream().forEach(vmWrapper -> assignedVms.put(vmWrapper.getVm(), vmWrapper.getAssignedTasksNumber()));

    LocalPlannerInputModel localPlannerModel = new LocalPlannerInputModel(level, assignedVms);
    return localPlannerModel;
  }

}
