package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;

import java.util.Map;

public class LocalPlannerInputModel {

  private Level level;
  private Map<Vm, Long> assignedVms;

  public LocalPlannerInputModel(Level level, Map<Vm, Long> assignedVms) {
    super();
    this.level = level;
    this.assignedVms = assignedVms;
  }

  public Level getLevel() {
    return level;
  }

  public Map<Vm, Long> getAssignedVms() {
    return assignedVms;
  }

}
