package pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SimulatorParameters {

  private String executionTimeModifier;
  private Boolean adjustToRealTime;

  public String getExecutionTimeModifier() {
    return executionTimeModifier;
  }

  public void setExecutionTimeModifier(String executionTimeModifier) {
    this.executionTimeModifier = executionTimeModifier;
  }

  public Boolean isAdjustToRealTime() {
    return adjustToRealTime;
  }

  public void setAdjustToRealTime(Boolean adjustToRealTime) {
    this.adjustToRealTime = adjustToRealTime;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("executionTimeModifier", executionTimeModifier)
        .append("adjustToRealTime", adjustToRealTime)
        .toString();
  }
}
