package pl.edu.agh.ki.mgr.workflowplanner.engine;

import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.IterationFinishedNotification;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.SimulationObserver;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.SimulationProgressNotification;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.SimulationStartedNotification;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.*;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class Log4jObserver implements SimulationObserver {

  public static final String HYPENS = "---------------------------------";
  public static final String STARS = "**********************************";

  @Override
  public void onProgressUpdate(SimulationProgressNotification notification) {
    LOGGER.info("UPDATE: " + notification.getDescription());
    switch (notification.getType()) {
      case SIMULATION_STARTED:
        print((SimulationStartedNotification) notification);
        break;
      case ITERATION_FINISHED:
        iterationFinished((IterationFinishedNotification) notification);
        break;
      default:
        break;
    }

  }

  private void print(SimulationStartedNotification notification) {
    InputDataModel inputDataModel = notification.getInputDataModel();

    info(HYPENS);
    info("Input data");
    info(HYPENS);
    info("Infrastructure:");
    info("VM Id | VM CCU | VM price");
    inputDataModel.getVms().forEach(vm -> {
      info(String.format("%-5d | %-6d | %-5d ", vm.getId(), vm.getCcu(), vm.getPricePerHour()));

    });
    info(HYPENS);

    info("Workflow:");
    info("Level Id | Average level CCU | Task count");
    inputDataModel.getLevels().forEach(level -> {
      info(String.format("%-8d | %-17d | %-5d ", level.getId(), level.getAverageRequiredCcu(), level.getTaskCount()));
    });

    info(HYPENS);
    info("Global deadline: " + inputDataModel.getGlobalDeadline());
    info(HYPENS);
  }

  private void iterationFinished(IterationFinishedNotification notification) {
    IterationResults iterationResults = notification.getIterationResults();

    globalPlanningResults(iterationResults.getWorkflowAssignmentResultsToDisplay());
    localPlanningResults(iterationResults.getLevelAssignmentResultsToDisplay());
    executionResults(iterationResults.getLevelExecutionResultsToDisplay());
    iterationResults(iterationResults, notification.getResultsCollector());
  }

  void globalPlanningResults(WorkflowAssignmentResultsToDisplay results) {
    debug(HYPENS);
    debug("Global planning results");
    debug("Used model: " + results.getGlobalPlanningModel().getDescription());
    debug("Estimate cost: " + results.getTotalEstimatedCost());
    debug("Estimate time: " + results.getTotalEstimatedTime());

    debug("Level Id | #Tasks | Avg CCU | Est time | Est cost | VMs[#Tasks]");

    results.getLevels().stream().forEach(level -> {
      String vmList = level.getAssignedVms().entrySet().stream().map(entry -> entry.getKey() + "[" + entry.getValue() + "]").sorted()//
          .reduce(null, (a, b) -> a == null ? b : a + ", " + b);
      String line = String.format("%-8d | %-6d | %-7d | %-8d | %-8d | %s", level.getLevelId(), level.getTasksCount(), level.getAverageLevelCCu(), level.getLevelTime(), level.getEstimatedCost(), vmList);
      debug(line);

    });

    debug(HYPENS);
  }

  void localPlanningResults(LevelAssignmentResultsToDisplay results) {
    debug(HYPENS);
    debug("Local planning results");
    debug("Level: " + results.getLevelId());
    debug("Estimate time: " + results.getEstimatedTime());
    debug("Estimate cost: " + results.getEstimatedCost());
    debug("TaskId  | TaskCCU | VmId | VmCCU | Est time | Est cost");
    results.getLevels().forEach(level -> {
      debug(String.format("%-7d | %-6d  | %-4d | %-5d | %-8d | %-3d", level.getTaskId(), level.getTaskCcu(), level.getVmId(), level.getVmCcu(), level.getEstimatedTime(), level.getEstimatedCost()));
    });

    debug(HYPENS);

  }

  void executionResults(LevelExecutionResultsToDisplay levelExecutionResultsToDisplay) {
    debug(HYPENS);
    debug("Level execution");
    debug("Level Id: " + levelExecutionResultsToDisplay.getLevelId());
    debug("Real cost: " + levelExecutionResultsToDisplay.getExecutionCost());
    debug("Real time: " + levelExecutionResultsToDisplay.getExecutionTime());

    debug("Task Id | VM Id | Real time | Real cost");
    levelExecutionResultsToDisplay.getTaskExecutionResultToDisplays()
        .forEach(r -> debug(
            String.format("%-7d | %-5d | %-9d | %-11d", r.getTaskId(), r.getVmId(), r.getExecutionTime(), r.getExecutionCost())));

    debug(HYPENS);

  }

  private void iterationResults(IterationResults iterationResults, ResultsCollector resultsCollector) {
    info(STARS);
    info("Iteration results");
    info("Iteration Id: " + iterationResults.getIterationId());
    info("Remaining time before scheduling: " + iterationResults.getRemainingTimeBeforeScheduling());
    info(HYPENS);
    info("Global planning results for all remaining level");
    WorkflowAssignmentResultsToDisplay workflowResults = iterationResults.getWorkflowAssignmentResultsToDisplay();
    final long globalEstimatedTime = workflowResults.getTotalEstimatedTime();
    final long globalEstimatedCost = workflowResults.getTotalEstimatedCost();
    info("Estimate time: " + globalEstimatedTime);
    info("Estimate cost: " + globalEstimatedCost);
    info(HYPENS);
    info("Local planning results for next level");
    LevelAssignmentResultsToDisplay levelResults = iterationResults.getLevelAssignmentResultsToDisplay();
    final long levelEstimatedTime = levelResults.getEstimatedTime();
    final long levelEstimatedCost = levelResults.getEstimatedCost();
    info("Estimate time: " + levelEstimatedTime);
    info("Estimate cost: " + levelEstimatedCost);
    info(HYPENS);
    info("Execution results for last level");
    LevelExecutionResultsToDisplay executionResults = iterationResults.getLevelExecutionResultsToDisplay();
    final Long executionTime = executionResults.getExecutionTime();
    final Long executionCost = executionResults.getExecutionCost();
    info("Execution time: " + executionTime);
    info("Execution cost: " + executionCost);
    info(HYPENS);

    final long estimatedLevelTimeFromGlobalPlanning = workflowResults.getLevels().get(0).getLevelTime();
    final long estimatedLevelCostFromGlobalPlanning = workflowResults.getLevels().get(0).getEstimatedCost();
    info("(level execution time - estimated time from global planning) = " + (executionTime - estimatedLevelTimeFromGlobalPlanning));
    info("(level execution time - estimated time from local planning)  = " + (executionTime - levelEstimatedTime));
    info("(level execution cost - estimated cost from global planning) = " + (executionCost - estimatedLevelCostFromGlobalPlanning));
    info("(level execution cost - estimated cost from local planning)  = " + (executionCost - levelEstimatedCost));
    info(HYPENS);
    info("Total time elapsed: " + resultsCollector.getElapsedRealTime());
    info("Total current cost: " + resultsCollector.getCurrentRealCost());
    info(STARS);
  }

  private void debug(String message) {
    LOGGER.info("> " + message);
  }

  private void info(String message) {
    LOGGER.info("> " + message);
  }

}
