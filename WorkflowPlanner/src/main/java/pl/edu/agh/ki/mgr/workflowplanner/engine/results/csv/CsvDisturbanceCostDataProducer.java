package pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv;

import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.DisturbanceEntry;

import java.util.ArrayList;
import java.util.List;

public class CsvDisturbanceCostDataProducer {
  public List<List<Object>> produceRecords(List<DisturbanceEntry> entries) {
    List<List<Object>> records = new ArrayList<>();

    for (DisturbanceEntry e : entries) {
      List<Object> entry = new ArrayList<>();
      entry.add(e.getDisturbance());
      entry.add(e.getAdaptiveCost());
      entry.add(e.getStaticCost());
      records.add(entry);
    }

    return records;
  }

  public List<String> produceHeaders() {
    List<String> headers = new ArrayList<>();
    headers.add("Disturbance");
    headers.add("Adaptive");
    headers.add("Static");

    return headers;
  }
}
