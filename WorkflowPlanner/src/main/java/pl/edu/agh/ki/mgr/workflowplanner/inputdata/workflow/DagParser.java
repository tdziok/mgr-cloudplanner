package pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class DagParser {

  public static final Pattern SPLIT = Pattern.compile("\\s+");

  public Dag parseDAG(String pathToDag) {
    Dag dag = new Dag();

    try (BufferedReader br = new BufferedReader(new FileReader(new File(pathToDag)))) {
      String line = null;
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (line.startsWith("#") || line.length() == 0) {
          continue;
        }

        String[] splitted = SPLIT.split(line);
        switch (splitted[0]) {
          case "TASK": {
            if (splitted.length != 4) {
              throw new IllegalArgumentException("Bad line format for TASK: " + splitted.toString());
            }

            String stringId = splitted[1];
            String name = splitted[2];
            double estimatedCcu = Double.parseDouble(splitted[3]);

            dag.addTask(stringId, name, estimatedCcu);
            break;
          }
          case "EDGE": {
            if (splitted.length != 3) {
              throw new IllegalArgumentException("Bad line format for EDGE: " + splitted.toString());
            }

            String parentId = splitted[1];
            String childId = splitted[2];

            dag.addEdge(parentId, childId);
            break;
          }
          default:
            break;
        }
      }
    } catch (IOException e) {
      LOGGER.error("Error when parsing DAG file", e);
      throw new RuntimeException("Error when parsing DAG file", e);
    }

    return dag;
  }

}
