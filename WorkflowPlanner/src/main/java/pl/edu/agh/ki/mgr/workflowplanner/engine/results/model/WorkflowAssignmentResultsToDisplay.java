package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel;

import java.util.List;

public class WorkflowAssignmentResultsToDisplay {

  private long totalEstimatedCost;
  private long totalEstimatedTime;
  private List<LevelEstimatedResultsToDisplay> levels;
  private GlobalPlanningModel globalPlanningModel;

  public long getTotalEstimatedCost() {
    return totalEstimatedCost;
  }

  public void setTotalEstimatedCost(long totalEstimatedCost) {
    this.totalEstimatedCost = totalEstimatedCost;
  }

  public long getTotalEstimatedTime() {
    return totalEstimatedTime;
  }

  public void setTotalEstimatedTime(long totalEstimatedTime) {
    this.totalEstimatedTime = totalEstimatedTime;
  }

  public List<LevelEstimatedResultsToDisplay> getLevels() {
    return levels;
  }

  public void setLevels(List<LevelEstimatedResultsToDisplay> levels) {
    this.levels = levels;
  }

  public GlobalPlanningModel getGlobalPlanningModel() {
    return globalPlanningModel;
  }

  public void setGlobalPlanningModel(GlobalPlanningModel globalPlanningModel) {
    this.globalPlanningModel = globalPlanningModel;
  }
}
