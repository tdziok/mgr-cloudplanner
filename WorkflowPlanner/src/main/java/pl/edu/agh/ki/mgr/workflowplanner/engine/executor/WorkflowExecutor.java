package pl.edu.agh.ki.mgr.workflowplanner.engine.executor;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifier;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ExecutionResults;

import java.util.HashMap;
import java.util.Map;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils.calculateComputationTime;

public class WorkflowExecutor {

  public ExecutionResults executeTasks(LevelAssignmentResult levelAssignmentResult, ExecutionTimeModifier executionTimeModifier) {

    Map<Vm, Long> vmTime = new HashMap<>();
    Map<Task, Double> taskTimeMap = new HashMap<>();

    levelAssignmentResult.getTaskToVm()
        .entrySet()
        .stream()
        .forEach(entry -> {
          Vm vm = entry.getValue();
          Task task = entry.getKey();
          Long taskTimeOnVm = calculateComputationTime(executionTimeModifier.getNextRandomForTask(task.getId()), task.getEstimatedResource(), vm.getCcu());

          if (vmTime.containsKey(vm)) {
            Long oldTime = vmTime.get(vm);
            vmTime.put(vm, oldTime + taskTimeOnVm);
          } else {
            vmTime.put(vm, taskTimeOnVm);
          }

          taskTimeMap.put(task, Double.valueOf(taskTimeOnVm));
        });

    Long executionTime = vmTime.values()
        .stream()
        .sorted((l1, l2) -> Integer.valueOf((int) (l2 - l1)))
        .findFirst()
        .get();

    return new ExecutionResults(levelAssignmentResult.getLevel(), taskTimeMap, vmTime, executionTime, new HashMap<>(levelAssignmentResult.getTaskToVm()));

  }
}
