package pl.edu.agh.ki.mgr.workflowplanner.engine.input;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;

import java.util.Collection;

public class InputDataDescription {

  private String workflowName;
  private Long tasksNumber;
  private Integer levelsNumber;

  private String vmSetName;
  private Integer vmNumber;

  private Long initGlobalDeadline;

  public InputDataDescription(InputDataModel data, String workflowName, String vmSetName) {
    this.withGlobalDeadline(data.getGlobalDeadline())
        .withLevelsNumber(data.getLevels().size())
        .withTasksNumber(data.getLevels()
            .stream()
            .map(Level::getTasks)
            .flatMap(Collection::stream)
            .count())
        .withVmNumber(data.getVms().size())
        .withWorkflowName(workflowName)
        .withVmSetName(vmSetName);
  }

  private InputDataDescription withWorkflowName(String workflowName) {
    this.workflowName = workflowName;
    return this;
  }

  private InputDataDescription withTasksNumber(Long tasksNumber) {
    this.tasksNumber = tasksNumber;
    return this;
  }

  private InputDataDescription withLevelsNumber(Integer levelsNumber) {
    this.levelsNumber = levelsNumber;
    return this;
  }

  private InputDataDescription withVmSetName(String vmSetName) {
    this.vmSetName = vmSetName;
    return this;
  }

  private InputDataDescription withVmNumber(Integer vmNumber) {
    this.vmNumber = vmNumber;
    return this;
  }

  private InputDataDescription withGlobalDeadline(Long globalDeadline) {
    this.initGlobalDeadline = globalDeadline;
    return this;
  }

  public String getWorkflowName() {
    return workflowName;
  }

  public String experimentName() {
    return new StringBuilder()
        .append(workflowName)
        .append("_D")
        .append(initGlobalDeadline)
        .toString()
        .replaceAll(" ", "_");
  }
}
