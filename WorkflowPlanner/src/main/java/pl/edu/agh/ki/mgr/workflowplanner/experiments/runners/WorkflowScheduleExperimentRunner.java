package pl.edu.agh.ki.mgr.workflowplanner.experiments.runners;

import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManager;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowScheduleExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.builder.ExperimentSimulatorBuilder;

public class WorkflowScheduleExperimentRunner {
  private final ExperimentSimulatorBuilder simulatorBuilder = new ExperimentSimulatorBuilder();

  public void execute(WorkflowScheduleExperiment workflowScheduleExperiment) {
    CloudManager simulation = simulatorBuilder.build(workflowScheduleExperiment);
    simulation.simulate();
  }
}
