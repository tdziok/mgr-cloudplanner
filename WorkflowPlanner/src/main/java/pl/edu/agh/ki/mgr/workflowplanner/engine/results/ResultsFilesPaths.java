package pl.edu.agh.ki.mgr.workflowplanner.engine.results;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ResultsFilesPaths {

  private static final String SEPARATOR = File.separator;
  private static final String CSV_RESULTS = "csvResults";
  private static final String _CSV = ".csv";
  private static final String PLOTS = "plots";
  private static final String WORKFLOW_TIME_DATA_PREFIX = "schedule_time_";
  private static final String WORKFLOW_COST_DATA_PREFIX = "schedule_cost_";
  private static final String WORKFLOW_DISTURBANCE_TIME_PREFIX = "disturbance_time_";
  private static final String WORKFLOW_DISTURBANCE_COST_PREFIX = "disturbance_cost_";
  private static final String WORKFLOW_COMPARISON_TIME_PREFIX = "comparison_time_";
  private static final String WORKFLOW_COMPARISON_COST_PREFIX = "comparison_cost_";

  private final String rootPath;
  private final String experimentName;
  private final String timestamp;

  public ResultsFilesPaths(String rootPath, String experimentName) {
    this.rootPath = rootPath;
    this.experimentName = experimentName;
    this.timestamp = new SimpleDateFormat("HH-mm-ss").format(new Date());
  }

  public String csvWithWorkflowTime() {
    return csvFile(WORKFLOW_TIME_DATA_PREFIX);
  }

  public String csvWithWorkflowCost() {
    return csvFile(WORKFLOW_COST_DATA_PREFIX);
  }

  public String csvWithWorkflowDisturbanceTime() {
    return csvFile(WORKFLOW_DISTURBANCE_TIME_PREFIX);
  }

  public String csvWithWorkflowDisturbanceCost() {
    return csvFile(WORKFLOW_DISTURBANCE_COST_PREFIX);
  }

  public String csvWithWorkflowComparisonTime() {
    return csvFile(WORKFLOW_COMPARISON_TIME_PREFIX);
  }

  public String csvWithWorkflowComparisonCost() {
    return csvFile(WORKFLOW_COMPARISON_COST_PREFIX);
  }

  public String plotWithWorkflowTime() {
    return plotFile(WORKFLOW_TIME_DATA_PREFIX);
  }

  public String plotWithWorkflowCost() {
    return plotFile(WORKFLOW_COST_DATA_PREFIX);
  }

  public String plotWithWorkflowDisturbanceTime() {
    return plotFile(WORKFLOW_DISTURBANCE_TIME_PREFIX);
  }

  public String plotWithWorkflowDisturbanceCost() {
    return plotFile(WORKFLOW_DISTURBANCE_COST_PREFIX);
  }

  public String plotWithWorkflowComparisonTime() {
    return plotFile(WORKFLOW_COMPARISON_TIME_PREFIX);
  }

  public String plotWithWorkflowComparisonCost() {
    return plotFile(WORKFLOW_COMPARISON_COST_PREFIX);
  }

  public String cmplLocal(int iteration) {
    return cmplRoot()
        .append("iteration-" + iteration + "-local")
        .toString();
  }

  public String cmplGlobalMain(int iteration) {
    return cmplRoot()
        .append("iteration-" + iteration + "-global-main")
        .toString();
  }

  public String cmplGlobalAlternative(int iteration) {
    return cmplRoot()
        .append("iteration-" + iteration + "-global-alternative")
        .toString();
  }

  private StringBuilder cmplRoot() {
    return rootOutput()
        .append("cmpl")
        .append(SEPARATOR);
  }

  public String loggerPath() {
    return rootOutput()
        .append("execution.log")
        .toString();
  }

  private String csvFile(String prefix) {
    return rootOutput()
        .append(CSV_RESULTS)
        .append(SEPARATOR)
        .append(prefix)
        .append(experimentName)
        .append(_CSV)
        .toString();
  }

  private String plotFile(String prefix) {
    return rootOutput()
        .append(PLOTS)
        .append(SEPARATOR)
        .append(prefix)
        .append(experimentName)
        .toString();
  }

  private StringBuilder rootOutput() {
    return new StringBuilder()
        .append(rootPath)
        .append(SEPARATOR)
        .append(experimentName)
        .append(SEPARATOR)
        .append(timestamp)
        .append(SEPARATOR);
  }

}
