package pl.edu.agh.ki.mgr.workflowplanner.engine.utils;

public class CalculationUtils {

  public static long calculateComputationTime(long taskCcu, long vmCcu) {
    return calculateComputationTime(1.0, taskCcu, vmCcu);
  }

  public static long calculateComputationCost(long time, long pricePerHour) {
    return time * pricePerHour;
  }

  public static long doubleToLong(double d) {
    return (long) Math.ceil(d);
  }

  public static Long calculateComputationTime(double random, long taskCcu, long vmCcu) {
    return (long) Math.ceil(random * taskCcu / vmCcu);
  }
}
