package pl.edu.agh.ki.mgr.workflowplanner.experiments.runners;

import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManager;
import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManagerParameters;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifier;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.GaussianExecutionTimeModifier;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.GaussianLongerExecutionTime;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.GaussianShorterExecutionTime;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataDescription;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.ResultsFilesPaths;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv.CsvWriter;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeCostExecResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeTimeExecResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.plots.RPlotGenerator;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowComparisonExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.builder.InputDataDescriptionBuilder;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.builder.InputDataModelBuilder;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class WorkflowComparisonExperimentRunner {

  public static final String WORKFLOW_COMPARISON = "workflowComparison";
  private final InputDataModelBuilder inputDataModelBuilder = new InputDataModelBuilder();
  private final InputDataDescriptionBuilder inputDataDescriptionBuilder = new InputDataDescriptionBuilder();

  public void execute(WorkflowComparisonExperiment experiment) {
    String rootPath = experiment.getResultsDirectory() + File.separator + WORKFLOW_COMPARISON + File.separator + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

    Result firstResult = executeWorkflow(experiment.getFirstWorkflowDescription(), experiment.getInfrastructureDescription(), experiment.getFirstWorkflowDeadline(), rootPath);
    Result secondResult = executeWorkflow(experiment.getSecondWorkflowDescription(), experiment.getInfrastructureDescription(), experiment.getSecondWorkflowDeadline(), rootPath);
    Result thirdResult = executeWorkflow(experiment.getThirdWorkflowDescription(), experiment.getInfrastructureDescription(), experiment.getThirdWorkflowDeadline(), rootPath);

    LOGGER.info("First results: " + firstResult);
    LOGGER.info("Second results: " + secondResult);
    LOGGER.info("Third results: " + thirdResult);

    final ResultsFilesPaths resultsFilesPaths = new ResultsFilesPaths(rootPath, WORKFLOW_COMPARISON);
    CsvWriter csvWriter = new CsvWriter(resultsFilesPaths);
    csvWriter.saveFileWithDataToWorkflowTimeComparison(Arrays.asList(firstResult.getTimeResults(), secondResult.getTimeResults(), thirdResult.getTimeResults()));
    csvWriter.saveFileWithDataToWorkflowCostComparison(Arrays.asList(firstResult.getCostResults(), secondResult.getCostResults(), thirdResult.getCostResults()));

    RPlotGenerator rPlotGenerator = new RPlotGenerator(resultsFilesPaths);
    rPlotGenerator.generateWorkflowComparisonTimePlot();
    rPlotGenerator.generateWorkflowComparisonCostPlot();
  }

  private Result executeWorkflow(WorkflowDescription workflow, InfrastructureDescription infrastructure, Long deadline, String outputDirectory) {
    final InputDataModel data = inputDataModelBuilder.build(workflow, infrastructure, deadline);
    final InputDataDescription description = inputDataDescriptionBuilder.build(data, workflow, infrastructure);

    return execute(data, description, outputDirectory);
  }

  private Result execute(InputDataModel data, InputDataDescription description, String resultsDirectory) {
    Long globalDeadline = data.getGlobalDeadline();

    ResultsCollector overEstimatedResults = execute(data, description, new GaussianShorterExecutionTime(), resultsDirectory);
    ResultsCollector exactEstimatedResults = execute(data, description, new GaussianExecutionTimeModifier(), resultsDirectory);
    ResultsCollector underEstimatedResults = execute(data, description, new GaussianLongerExecutionTime(), resultsDirectory);

    double overEstimatedExecution = (double) overEstimatedResults.getElapsedRealTime() / globalDeadline;
    double exactEstimatedTime = (double) exactEstimatedResults.getElapsedRealTime() / globalDeadline;
    double underEstimatedTime = (double) underEstimatedResults.getElapsedRealTime() / globalDeadline;

    WorkflowThreeTimeExecResults timeResults = new WorkflowThreeTimeExecResults(description.getWorkflowName(), overEstimatedExecution, exactEstimatedTime, underEstimatedTime);

    double exactEstimatedTimeCost = (double) exactEstimatedResults.getCurrentRealCost() / exactEstimatedResults.getCurrentRealCost();
    double overEstimatedExecutionCost = (double) overEstimatedResults.getCurrentRealCost() / exactEstimatedResults.getCurrentRealCost();
    double underEstimatedTimeCost = (double) underEstimatedResults.getCurrentRealCost() / exactEstimatedResults.getCurrentRealCost();

    WorkflowThreeCostExecResults costResults = new WorkflowThreeCostExecResults(description.getWorkflowName(), overEstimatedExecutionCost, exactEstimatedTimeCost, underEstimatedTimeCost);
    return new Result(timeResults, costResults);
  }

  private ResultsCollector execute(InputDataModel data, InputDataDescription description, ExecutionTimeModifier executionTimeModifier, String resultsDirectory) {

    CloudManagerParameters cloudManagerParameters = new CloudManagerParameters(executionTimeModifier, true, resultsDirectory);
    return new CloudManager(cloudManagerParameters, data, description, "workflowComparison").simulate();

  }

  private static class Result {

    private final WorkflowThreeTimeExecResults timeResults;
    private final WorkflowThreeCostExecResults costResults;

    public Result(WorkflowThreeTimeExecResults timeResults, WorkflowThreeCostExecResults costResults) {
      this.timeResults = timeResults;
      this.costResults = costResults;
    }

    public WorkflowThreeTimeExecResults getTimeResults() {
      return timeResults;
    }

    public WorkflowThreeCostExecResults getCostResults() {
      return costResults;
    }

    @Override
    public String toString() {
      return "Result{" +
          "timeResults=" + timeResults +
          ", costResults=" + costResults +
          '}';
    }
  }
}
