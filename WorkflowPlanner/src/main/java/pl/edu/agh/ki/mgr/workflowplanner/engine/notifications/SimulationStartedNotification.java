package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.SIMULATION_STARTED;

public class SimulationStartedNotification extends AbstractSimulationNotification {

  private final InputDataModel inputDataModel;

  public SimulationStartedNotification(InputDataModel inputDataModel) {
    super(SIMULATION_STARTED);
    this.inputDataModel = inputDataModel;
  }

  public InputDataModel getInputDataModel() {
    return inputDataModel;
  }
}
