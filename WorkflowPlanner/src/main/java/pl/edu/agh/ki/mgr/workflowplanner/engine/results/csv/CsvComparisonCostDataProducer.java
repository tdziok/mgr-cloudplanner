package pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv;

import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeCostExecResults;

import java.util.ArrayList;
import java.util.List;

public class CsvComparisonCostDataProducer {
  public List<List<Object>> records(List<WorkflowThreeCostExecResults> workflowThreeCostExecResults) {
    List<List<Object>> records = new ArrayList<>();

    for (WorkflowThreeCostExecResults e : workflowThreeCostExecResults) {
      List<Object> entry = new ArrayList<>();
      entry.add(e.getWorkflowName());
      entry.add(e.getOverEstimatedExecutionCost());
      entry.add(e.getExactEstimatedExecutionCost());
      entry.add(e.getUnderEstimatedExecutionCost());
      records.add(entry);
    }

    return records;
  }

  public List<String> headers() {
    List<String> headers = new ArrayList<>();
    headers.add("Workflow");
    headers.add("Overestimation");
    headers.add("Exact estimation");
    headers.add("Underestimation");

    return headers;
  }
}
