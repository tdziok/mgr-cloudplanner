package pl.edu.agh.ki.mgr.workflowplanner.experiments.runners;

import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManager;
import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManagerParameters;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.GaussianParametrizedExecutionTimeModifier;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataDescription;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.ResultsFilesPaths;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv.CsvWriter;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.DisturbanceEntry;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.plots.RPlotGenerator;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowDisturbanceExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.builder.InputDataModelBuilder;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class WorkflowDisturbanceExperimentRunner {

  public static final String WORKFLOW_DISTURBANCE = "workflowDisturbance";

  private final InputDataModelBuilder inputDataModelBuilder = new InputDataModelBuilder();

  public void execute(WorkflowDisturbanceExperiment workflowDisturbanceExperiment) {
    String rootPath = workflowDisturbanceExperiment.getResultsDirectory() + File.separator + WORKFLOW_DISTURBANCE + File.separator + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

    Long deadline = workflowDisturbanceExperiment.getDeadline();
    InfrastructureDescription infrastructureDescription = workflowDisturbanceExperiment.getInfrastructureDescription();
    WorkflowDescription workflowDescription = workflowDisturbanceExperiment.getWorkflowDescription();
    InputDataModel data = inputDataModelBuilder.build(workflowDescription, infrastructureDescription, deadline);

    InputDataDescription dataDescription = new InputDataDescription(data, workflowDescription.getName(), infrastructureDescription.getName());

    List<Double> modifiers = new ArrayList<>();
    double m = 0.75;
    while (m <= 1.26) {
      modifiers.add(m);
      m += 0.05;
    }

    List<DisturbanceEntry> timeEntries = new ArrayList<>();

    for (double modifier : modifiers) {
      ResultsCollector adaptiveResults = executeWorkflow(dataDescription, rootPath, data, modifier, true);
      ResultsCollector staticResults = executeWorkflow(dataDescription, rootPath, data, modifier, false);

      DisturbanceEntry disturbanceEntry = new DisturbanceEntry()
          .withDisturbance(modifier)
          .withAdaptiveTime(adaptiveResults.getElapsedRealTime())
          .withAdaptiveCost(adaptiveResults.getCurrentRealCost())
          .withStaticTime(staticResults.getElapsedRealTime())
          .withStaticCost(staticResults.getCurrentRealCost());

      LOGGER.info("Disturbance entry: " + disturbanceEntry);
      timeEntries.add(disturbanceEntry);

    }
    LOGGER.info("Saving disturbance csv results");
    final ResultsFilesPaths resultsFilesPaths = new ResultsFilesPaths(rootPath, WORKFLOW_DISTURBANCE);
    new CsvWriter(resultsFilesPaths).saveFileWithDisturbanceTime(timeEntries);
    new CsvWriter(resultsFilesPaths).saveFileWithDisturbanceCost(timeEntries);

    LOGGER.info("Saving disturbance plots");
    new RPlotGenerator(resultsFilesPaths).generateDisturbanceTimePlot(data.getGlobalDeadline());
    new RPlotGenerator(resultsFilesPaths).generateDisturbanceCostPlot();
  }

  private ResultsCollector executeWorkflow(InputDataDescription dataDescription, String rootPath, InputDataModel data, double modifier, boolean shouldTakeIntoAccountCurrentTime) {
    CloudManagerParameters params = new CloudManagerParameters(new GaussianParametrizedExecutionTimeModifier(modifier), shouldTakeIntoAccountCurrentTime, rootPath);

    return new CloudManager(params, data, dataDescription, "workflowDisturbance")
        .simulate();
  }
}
