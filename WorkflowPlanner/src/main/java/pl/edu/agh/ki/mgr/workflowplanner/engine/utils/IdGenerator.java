package pl.edu.agh.ki.mgr.workflowplanner.engine.utils;

public class IdGenerator {

  private long nextValue = 0;

  public long nextValue() {
    return nextValue++;
  }

}
