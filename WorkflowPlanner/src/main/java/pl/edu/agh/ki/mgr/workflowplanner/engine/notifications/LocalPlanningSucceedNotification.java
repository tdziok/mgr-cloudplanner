package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.LOCAL_PLANNING_SUCCEED;

public class LocalPlanningSucceedNotification extends AbstractLevelAwareNotification {

  public LocalPlanningSucceedNotification(int level) {
    super(LOCAL_PLANNING_SUCCEED, level);
  }
}
