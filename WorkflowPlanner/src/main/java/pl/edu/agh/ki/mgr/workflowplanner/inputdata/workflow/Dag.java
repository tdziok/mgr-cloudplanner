package pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Dag {

  private Map<String, TaskNode> tasks = new HashMap<>();
  private long nextTaskId = 0;

  public TaskNode addTask(String id, String name, double ccu) {
    TaskNode task = new TaskNode(nextTaskId++, id, name, ccu);
    tasks.put(id, task);
    return task;
  }

  public Set<TaskNode> getTasks() {
    return new HashSet<>(tasks.values());
  }

  public void addEdge(String parentId, String childId) {
    TaskNode parent = tasks.get(parentId);
    TaskNode child = tasks.get(childId);

    parent.addChild(child);
    child.addParent(parent);
  }

}
