package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;

import java.util.HashMap;
import java.util.Map;

public class LevelAssignmentResult {

  private final boolean succeed;
  private final Exception exception;
  private final Level level;
  private Map<Task, Vm> taskToVm = new HashMap<>();

  private LevelAssignmentResult(Level level, boolean succeed, Exception exception) {
    this.level = level;
    this.succeed = succeed;
    this.exception = exception;
  }

  public static LevelAssignmentResult succeedResult(Level level) {
    return new LevelAssignmentResult(level, true, null);
  }

  public static LevelAssignmentResult failureResult(Exception exception) {
    return new LevelAssignmentResult(null, false, exception);
  }

  public boolean isSucceed() {
    return succeed;
  }

  public Exception getException() {
    return exception;
  }

  public Level getLevel() {
    return level;
  }

  public void setAssignment(Task task, Vm vm) {
    taskToVm.put(task, vm);
  }

  public Map<Task, Vm> getTaskToVm() {
    return taskToVm;
  }

}
