package pl.edu.agh.ki.mgr.workflowplanner.experiments.builder;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Infrastructure;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Workflow;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;
import pl.edu.agh.ki.mgr.workflowplanner.inputdata.infrastructure.InfrastructureReader;
import pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow.Dag;
import pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow.DagParser;
import pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow.DagTranslator;
import pl.edu.agh.ki.mgr.workflowplanner.utils.BadInputException;

import java.io.File;
import java.util.List;

public class InputDataModelBuilder {
  public InputDataModel build(WorkflowDescription workflowDescription, InfrastructureDescription infrastructureDescription, Long deadline) {
    final String pathToWorkflowDagFile = workflowDescription.getFilePath();

    final DagParser dagParser = new DagParser();
    final Dag dag = dagParser.parseDAG(pathToWorkflowDagFile);
    final DagTranslator dagTranslator = new DagTranslator();
    final List<Level> levels = dagTranslator.translateFromDag(dag);

    final String pathToInfrastructureJsonFile = infrastructureDescription.getFilePath();
    final InfrastructureReader infrastructureReader = new InfrastructureReader();
    try {
      final List<Vm> vmListFromJson = infrastructureReader.readVmListFromJson(new File(pathToInfrastructureJsonFile));
      final Infrastructure infrastructure = new Infrastructure(vmListFromJson);
      final Workflow workflow = new Workflow(levels);
      return new InputDataModel(deadline, infrastructure, workflow);
    } catch (Exception e) {
      throw new BadInputException("Error when reading infrastructure data from file: " + pathToInfrastructureJsonFile, e);
    }
  }
}
