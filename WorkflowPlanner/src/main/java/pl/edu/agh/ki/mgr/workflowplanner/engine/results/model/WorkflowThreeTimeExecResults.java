package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class WorkflowThreeTimeExecResults {

  private final String workflowName;
  private final double overEstimatedExecutionTime;
  private final double exactEstimatedExecutionTime;
  private final double underEstimatedExecutionTime;

  public WorkflowThreeTimeExecResults(String workflowName, double overEstimatedExecutionTime, double exactEstimatedExecutionTime, double underEstimatedExecutionTime) {
    this.workflowName = workflowName;
    this.overEstimatedExecutionTime = overEstimatedExecutionTime;
    this.exactEstimatedExecutionTime = exactEstimatedExecutionTime;
    this.underEstimatedExecutionTime = underEstimatedExecutionTime;
  }

  public String getWorkflowName() {
    return workflowName;
  }

  public double getOverEstimatedExecutionTime() {
    return overEstimatedExecutionTime;
  }

  public double getExactEstimatedExecutionTime() {
    return exactEstimatedExecutionTime;
  }

  public double getUnderEstimatedExecutionTime() {
    return underEstimatedExecutionTime;
  }

  @Override
  public String toString() {
    return "WorkflowThreeTimeExecResults{" +
        "workflowName='" + workflowName + '\'' +
        ", overEstimatedExecutionTime=" + overEstimatedExecutionTime +
        ", exactEstimatedExecutionTime=" + exactEstimatedExecutionTime +
        ", underEstimatedExecutionTime=" + underEstimatedExecutionTime +
        '}';
  }
}
