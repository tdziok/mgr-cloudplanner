package pl.edu.agh.ki.mgr.workflowplanner.sampleData;

import pl.edu.agh.ki.mgr.workflowplanner.domain.*;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.IdGenerator;

import java.util.ArrayList;
import java.util.List;

public class FlowForPaper_2_2_1 {

  public InputDataModel getData() {
    IdGenerator levelsIdGenerator = new IdGenerator();
    IdGenerator tasksIdGenerator = new IdGenerator();

    List<Level> levels = new ArrayList<>();
    levels.add(new Level() //
        .withId(levelsIdGenerator.nextValue()) //
        .withTask( //
            new Task() //
                .withLongId(tasksIdGenerator.nextValue()) //
                .withEstimatedResource(22L) //
                .withName("preprocess") //
        ) //
        .withTask( //
            new Task() //
                .withLongId(tasksIdGenerator.nextValue()) //
                .withEstimatedResource(18L) //
                .withName("preprocess") //
        ) //
    );

    levels.add(new Level() //
        .withId(levelsIdGenerator.nextValue()) //
        .withTask( //
            new Task() //
                .withLongId(tasksIdGenerator.nextValue()) //
                .withEstimatedResource(10L) //
                .withName("calculate") //
        ) //
        .withTask( //
            new Task() //
                .withLongId(tasksIdGenerator.nextValue()) //
                .withEstimatedResource(10L) //
                .withName("calculate") //
        ) //
    );

    levels.add(new Level() //
        .withId(levelsIdGenerator.nextValue()) //
        .withTask( //
            new Task() //
                .withLongId(tasksIdGenerator.nextValue()) //
                .withEstimatedResource(20L) //
                .withName("reduce") //
        ) //
    );

    List<Vm> vms = new ArrayList<>();
    vms.add(new Vm().withId(1L).withCcu(5L).withPricePerHour(10L).withName("small"));
    vms.add(new Vm().withId(2L).withCcu(10L).withPricePerHour(25L).withName("medium"));

    InputDataModel inputModel = new InputDataModel(15L, new Infrastructure(vms), new Workflow(levels));
    return inputModel;
  }
}
