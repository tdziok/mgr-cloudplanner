package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class WorkflowThreeCostExecResults {

  private final String workflowName;
  private final double overEstimatedExecutionCost;
  private final double exactEstimatedExecutionCost;
  private final double underEstimatedExecutionCost;

  public WorkflowThreeCostExecResults(String workflowName, double overEstimatedExecutionCost, double exactEstimatedExecutionCost, double underEstimatedExecutionCost) {
    this.workflowName = workflowName;
    this.overEstimatedExecutionCost = overEstimatedExecutionCost;
    this.exactEstimatedExecutionCost = exactEstimatedExecutionCost;
    this.underEstimatedExecutionCost = underEstimatedExecutionCost;
  }

  @Override
  public String toString() {
    return "WorkflowThreeCostExecResults{" +
        "workflowName='" + workflowName + '\'' +
        ", overEstimatedExecutionCost=" + overEstimatedExecutionCost +
        ", exactEstimatedExecutionCost=" + exactEstimatedExecutionCost +
        ", underEstimatedExecutionCost=" + underEstimatedExecutionCost +
        '}';
  }

  public String getWorkflowName() {
    return workflowName;
  }

  public double getOverEstimatedExecutionCost() {
    return overEstimatedExecutionCost;
  }

  public double getExactEstimatedExecutionCost() {
    return exactEstimatedExecutionCost;
  }

  public double getUnderEstimatedExecutionCost() {
    return underEstimatedExecutionCost;
  }
}
