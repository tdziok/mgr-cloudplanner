package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class GlobalPlanningFailedNotification extends AbstractLevelAwareNotification {
  private final Exception exception;

  public GlobalPlanningFailedNotification(int level, Exception exception) {
    super(NotificationType.GLOBAL_PLANNING_FAILED, level);
    this.exception = exception;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Error: " + exception.toString();
  }
}
