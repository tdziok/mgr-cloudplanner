package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;

import java.util.Map;

public class ExecutionResults {

  private Level level;
  private Map<Task, Double> taskTimeMap;
  private Map<Vm, Long> vmTimeMap;
  private Map<Task, Vm> taskToVmMap;
  private Long realExecutionTime;

  public Level getLevel() {
    return level;
  }

  public Map<Task, Double> getTaskTimeMap() {
    return taskTimeMap;
  }

  public Map<Vm, Long> getVmTimeMap() {
    return vmTimeMap;
  }

  public Map<Task, Vm> getTaskToVmMap() {
    return taskToVmMap;
  }

  public ExecutionResults(Level level, Map<Task, Double> taskTimeMap, Map<Vm, Long> vmTime, Long executionTime, Map<Task, Vm> taskToVmMap) {

    this.level = level;
    this.taskTimeMap = taskTimeMap;
    this.vmTimeMap = vmTime;
    this.realExecutionTime = executionTime;
    this.taskToVmMap = taskToVmMap;
  }

  public Long getLevelId() {
    return level.getId();
  }

  public Long getRealExecutionTime() {
    return realExecutionTime;
  }

}
