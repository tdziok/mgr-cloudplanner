package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

abstract class AbstractLevelAwareNotification extends AbstractSimulationNotification {
  private final int level;

  protected AbstractLevelAwareNotification(NotificationType notificationType, int level) {
    super(notificationType);
    this.level = level;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Level: " + level;
  }
}
