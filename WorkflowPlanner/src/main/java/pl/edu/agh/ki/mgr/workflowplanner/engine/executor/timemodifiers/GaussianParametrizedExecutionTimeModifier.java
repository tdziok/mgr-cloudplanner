package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

public class GaussianParametrizedExecutionTimeModifier extends GaussianExecutionTimeModifier {
  private final double modifier;

  public GaussianParametrizedExecutionTimeModifier(double modifier) {
    this.modifier = modifier;
  }

  @Override
  public double getNextRandom() {
    return super.getNextRandom() * modifier;
  }

  @Override
  public String getName() {
    return super.getName() + "-" + modifier;
  }
}
