package pl.edu.agh.ki.mgr.workflowplanner.domain;

public class Task implements Comparable<Task> {

  private Long longId;
  private String name;
  private Long estimatedResource;

  public Task() {
  }

  public Task(Long id) {
    this.longId = id;
  }

  public Task withLongId(Long id) {
    this.longId = id;
    return this;
  }

  public Long getId() {
    return longId;
  }

  public Task withEstimatedResource(Long estimatedResource) {
    this.estimatedResource = estimatedResource;
    return this;
  }

  public Long getEstimatedResource() {
    return estimatedResource;
  }

  public Task withName(String name) {
    this.name = name;
    return this;
  }

  public String getName() {
    return name;
  }

  @Override
  public int compareTo(Task o) {
    long diff = this.getId() - o.getId();
    return (int) diff;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((longId == null) ? 0 : longId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Task other = (Task) obj;
    if (longId == null) {
      if (other.longId != null) return false;
    } else if (!longId.equals(other.longId)) return false;
    return true;
  }

}
