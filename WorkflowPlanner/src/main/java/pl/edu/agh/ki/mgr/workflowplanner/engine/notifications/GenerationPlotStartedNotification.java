package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class GenerationPlotStartedNotification extends AbstractSimulationNotification {
  public GenerationPlotStartedNotification() {
    super(NotificationType.PLOTS_GENERATION_STARTED);
  }
}
