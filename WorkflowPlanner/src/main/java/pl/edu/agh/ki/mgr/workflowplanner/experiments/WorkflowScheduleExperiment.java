package pl.edu.agh.ki.mgr.workflowplanner.experiments;

import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.SimulatorParameters;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

public class WorkflowScheduleExperiment {
  private SimulatorParameters simulatorParameters;
  private InfrastructureDescription infrastructureDescription;
  private WorkflowDescription workflowDescription;

  private Long deadline;

  private String resultsDirectory;

  public SimulatorParameters getSimulatorParameters() {
    return simulatorParameters;
  }

  public void setSimulatorParameters(SimulatorParameters simulatorParameters) {
    this.simulatorParameters = simulatorParameters;
  }

  public InfrastructureDescription getInfrastructureDescription() {
    return infrastructureDescription;
  }

  public void setInfrastructureDescription(InfrastructureDescription infrastructureDescription) {
    this.infrastructureDescription = infrastructureDescription;
  }

  public WorkflowDescription getWorkflowDescription() {
    return workflowDescription;
  }

  public void setWorkflowDescription(WorkflowDescription workflowDescription) {
    this.workflowDescription = workflowDescription;
  }

  public Long getDeadline() {
    return deadline;
  }

  public void setDeadline(Long deadline) {
    this.deadline = deadline;
  }

  public String getResultsDirectory() {
    return resultsDirectory;
  }

  public void setResultsDirectory(String resultsDirectory) {
    this.resultsDirectory = resultsDirectory;
  }

  @Override
  public String toString() {
    return new StringBuilder()
        .append("\n")
        .append(simulatorParameters)
        .append("\n")
        .append(infrastructureDescription)
        .append("\n")
        .append( workflowDescription)
        .append("\n")
        .append("Deadline " + deadline)
        .append("\n")
        .append("ResultsDirectory " + resultsDirectory)
        .toString();
  }
}
