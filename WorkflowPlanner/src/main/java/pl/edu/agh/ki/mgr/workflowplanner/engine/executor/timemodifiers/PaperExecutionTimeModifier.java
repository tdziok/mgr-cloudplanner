package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

import java.util.HashMap;
import java.util.Map;

public class PaperExecutionTimeModifier implements ExecutionTimeModifier {

  private static Map<Long, Double> executionTimeModifier = new HashMap<>();

  static {
    executionTimeModifier.put(0L, 0.5);
    executionTimeModifier.put(1L, 0.5);
    executionTimeModifier.put(2L, 2.0);
    executionTimeModifier.put(3L, 2.0);
    executionTimeModifier.put(4L, 1.0);
  }

  @Override
  public double getNextRandom() {
    return 1;
  }

  @Override
  public double getNextRandomForTask(long taskId) {
    return executionTimeModifier.get(taskId);
  }

  @Override
  public String getName() {
    return "PaperRandomizer";
  }
}
