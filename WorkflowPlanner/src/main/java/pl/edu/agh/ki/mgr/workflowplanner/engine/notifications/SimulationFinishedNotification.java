package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.SIMULATION_FINISHED;

public class SimulationFinishedNotification extends AbstractSimulationNotification {

  public SimulationFinishedNotification() {
    super(SIMULATION_FINISHED);
  }
}
