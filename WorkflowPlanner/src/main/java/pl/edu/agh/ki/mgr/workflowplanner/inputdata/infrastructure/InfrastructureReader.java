package pl.edu.agh.ki.mgr.workflowplanner.inputdata.infrastructure;

import com.google.gson.Gson;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.IdGenerator;
import pl.edu.agh.ki.mgr.workflowplanner.utils.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class InfrastructureReader {

  public List<Vm> readVmListFromJson(File jsonFile) throws Exception {
    Gson gson = new Gson();
    VmInfo array[] = gson.fromJson(new BufferedReader(new FileReader(jsonFile)), VmInfo[].class);
    List<VmInfo> vmInfoList = Arrays.asList(array);

    final IdGenerator idGenerator = new IdGenerator();
    return vmInfoList.stream()
        .map(vmInfo -> vmInfoToVmsList(vmInfo, idGenerator))
        .flatMap(Collection::stream)
        .collect(toList());
  }

  private List<Vm> vmInfoToVmsList(VmInfo vmInfo, IdGenerator idGenerator) {
    Double tmp = 1.0;
    try {
      tmp = Double.parseDouble(vmInfo.getECU());
    } catch (Exception e) {
      Logger.LOGGER.info("Unable to parse ecu for vm, setting default value");
    }
    final Double ccu = tmp;

    return IntStream.range(0, (int) vmInfo.getAVAILABLEINSTANCES())
        .boxed()
        .map(ignored -> new Vm()
            .withId(idGenerator.nextValue())
            .withName(vmInfo.getNAME())
            .withCcu(ccu.longValue())
            .withPricePerHour(vmInfo.getCENTSPERHOUR()))
        .collect(toList());
  }

}
