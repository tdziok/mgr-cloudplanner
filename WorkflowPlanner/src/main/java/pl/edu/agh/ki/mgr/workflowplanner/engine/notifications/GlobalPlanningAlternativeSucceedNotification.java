package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.GLOBAL_PLANNING_ALTERNATIVE_SUCCEED;

public class GlobalPlanningAlternativeSucceedNotification extends AbstractLevelAwareNotification {
  public GlobalPlanningAlternativeSucceedNotification(int level) {
    super(GLOBAL_PLANNING_ALTERNATIVE_SUCCEED, level);
  }
}
