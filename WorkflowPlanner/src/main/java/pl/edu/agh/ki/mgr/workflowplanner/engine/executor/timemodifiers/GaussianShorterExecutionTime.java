package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

public class GaussianShorterExecutionTime extends GaussianExecutionTimeModifier {

  @Override
  public double getNextRandom() {
    return super.getNextRandom() * 0.75;
  }

  @Override
  public String getName() {
    return "GaussianShorter";
  }
}
