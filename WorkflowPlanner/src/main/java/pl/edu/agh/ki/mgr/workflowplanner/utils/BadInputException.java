package pl.edu.agh.ki.mgr.workflowplanner.utils;

public class BadInputException extends RuntimeException {

  public BadInputException(String message) {
    super(message);
  }

  public BadInputException(String message, Exception e) {
    super(message, e);
  }
}
