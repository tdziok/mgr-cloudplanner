package pl.edu.agh.ki.mgr.workflowplanner.domain;

import java.util.List;

public class Workflow {

  private final List<Level> levels;

  public Workflow(List<Level> levels) {
    this.levels = levels;
  }

  public List<Level> getLevels() {
    return levels;
  }
}
