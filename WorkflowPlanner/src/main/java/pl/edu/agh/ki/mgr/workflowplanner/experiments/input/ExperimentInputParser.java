package pl.edu.agh.ki.mgr.workflowplanner.experiments.input;

import com.google.gson.Gson;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.ExperimentsInput;
import pl.edu.agh.ki.mgr.workflowplanner.utils.BadInputException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExperimentInputParser {
  public ExperimentsInput parse(String configJsonPath) {
    Gson gson = new Gson();
    try {
      return gson.fromJson(new BufferedReader(new FileReader(configJsonPath)), ExperimentsInput.class);
    } catch (FileNotFoundException e) {
      throw new BadInputException("Error when reading experiments config from file: " + configJsonPath, e);
    }
  }
}
