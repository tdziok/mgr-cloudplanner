package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public interface SimulationProgressNotification {
  NotificationType getType();

  String getDescription();
}
