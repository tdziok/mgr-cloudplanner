package pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.ResultsFilesPaths;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.DisturbanceEntry;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeCostExecResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeTimeExecResults;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class CsvWriter {

  private final ResultsFilesPaths resultsFilesPaths;

  private final CsvCostPlotDataProducer costPlotDataProducer = new CsvCostPlotDataProducer();
  private final CsvTimePlotDataProducer timePlotDataProducer = new CsvTimePlotDataProducer();
  private final CsvDisturbanceCostDataProducer disturbanceCostDataProducer = new CsvDisturbanceCostDataProducer();
  private final CsvDisturbanceTimeDataProducer disturbanceTimeDataProducer = new CsvDisturbanceTimeDataProducer();
  private final CsvComparisonCostDataProducer comparisonCostDataProducer = new CsvComparisonCostDataProducer();
  private final CsvComparisonTimeDataProducer comparisonTimeDataProducer = new CsvComparisonTimeDataProducer();

  public CsvWriter(ResultsFilesPaths resultsFilesPaths) {
    this.resultsFilesPaths = resultsFilesPaths;
  }

  public void writeResultsForCostPlot(ResultsCollector resultsCollector) {
    final List<List<Object>> records = costPlotDataProducer.produceData(resultsCollector);
    final List<String> headers = costPlotDataProducer.produceHeaders(resultsCollector);

    final String csvPath = resultsFilesPaths.csvWithWorkflowCost();
    final String[] csvHeaders = headers.toArray(new String[headers.size()]);
    final File pathToFile = new File(csvPath);

    writeToCsv(records, csvHeaders, pathToFile);
  }

  public void writeResultsForTimePlot(ResultsCollector resultsCollector) {
    List<List<Object>> records = timePlotDataProducer.produceData(resultsCollector);
    List<String> headers = timePlotDataProducer.produceHeaders(resultsCollector);

    final String csvPath = resultsFilesPaths.csvWithWorkflowTime();
    final String[] csvHeaders = headers.toArray(new String[headers.size()]);
    final File pathToFile = new File(csvPath);

    writeToCsv(records, csvHeaders, pathToFile);
  }

  public void saveFileWithDisturbanceTime(List<DisturbanceEntry> entries) {
    List<List<Object>> records = disturbanceTimeDataProducer.produceRecords(entries);
    List<String> headers = disturbanceTimeDataProducer.produceHeaders();

    final String[] csvHeaders = headers.toArray(new String[headers.size()]);
    final File pathToFile = new File(resultsFilesPaths.csvWithWorkflowDisturbanceTime());

    writeToCsv(records,
        csvHeaders,
        pathToFile);
  }

  public void saveFileWithDisturbanceCost(List<DisturbanceEntry> entries) {
    List<List<Object>> records = disturbanceCostDataProducer.produceRecords(entries);
    List<String> headers = disturbanceCostDataProducer.produceHeaders();

    final String csvPath = resultsFilesPaths.csvWithWorkflowDisturbanceCost();
    final File pathToFile = new File(csvPath);

    writeToCsv(records,
        headers.toArray(new String[headers.size()]),
        pathToFile);
  }

  public void saveFileWithDataToWorkflowTimeComparison(List<WorkflowThreeTimeExecResults> workflowThreeTimeExecResults) {
    List<List<Object>> records = comparisonTimeDataProducer.records(workflowThreeTimeExecResults);
    List<String> headers = comparisonTimeDataProducer.headers();

    final String csvPath = resultsFilesPaths.csvWithWorkflowComparisonTime();
    final File pathToFile = new File(csvPath);

    writeToCsv(records,
        headers.toArray(new String[headers.size()]),
        pathToFile);
  }

  public void saveFileWithDataToWorkflowCostComparison(List<WorkflowThreeCostExecResults> workflowThreeCostExecResults) {
    List<List<Object>> records = comparisonCostDataProducer.records(workflowThreeCostExecResults);
    List<String> headers = comparisonCostDataProducer.headers();

    final String csvPath = resultsFilesPaths.csvWithWorkflowComparisonCost();
    final File pathToFile = new File(csvPath);

    writeToCsv(records,
        headers.toArray(new String[headers.size()]),
        pathToFile);
  }

  private void writeToCsv(List<List<Object>> records, String[] headers, File pathToFile) {
    try {
      pathToFile.getParentFile().mkdirs();
      final BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFile));
      CSVPrinter printer = CSVFormat.DEFAULT.withHeader(headers).print(writer);
      for (List<?> record : records) {
        printer.printRecord(record);
      }
      printer.flush();

    } catch (IOException e) {
      LOGGER.error("Error when saving csv file", e);
    }
  }

}
