package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplConstants;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel;

public class GlobalPlannerWithoutDeadline extends WorkflowGlobalPhasePlanner {
  public GlobalPlannerWithoutDeadline() {
    super(CmplConstants.CMLP_WORKFLOW_MODEL_MIN_TIME_PATH, GlobalPlanningModel.ALTERNATIVE_MINIMIZE_TIME);
  }
}
