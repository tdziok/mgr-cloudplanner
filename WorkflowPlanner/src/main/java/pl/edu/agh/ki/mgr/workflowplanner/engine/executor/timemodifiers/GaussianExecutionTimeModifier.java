package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

import java.util.Random;

public class GaussianExecutionTimeModifier implements ExecutionTimeModifier {

  private Random random = new Random(System.currentTimeMillis());

  @Override
  public double getNextRandom() {
    return 1.0 + random.nextGaussian() / 4;
  }

  @Override
  public String getName() {
    return "Gaussian";
  }

}
