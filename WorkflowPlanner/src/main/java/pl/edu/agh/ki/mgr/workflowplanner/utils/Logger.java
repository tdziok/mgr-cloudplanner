package pl.edu.agh.ki.mgr.workflowplanner.utils;

import org.slf4j.LoggerFactory;

public class Logger {

  public static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Logger.class);

}
