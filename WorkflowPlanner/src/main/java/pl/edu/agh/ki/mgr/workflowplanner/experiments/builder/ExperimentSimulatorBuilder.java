package pl.edu.agh.ki.mgr.workflowplanner.experiments.builder;

import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManager;
import pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager.CloudManagerParameters;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifier;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifiers;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataDescription;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowScheduleExperiment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExperimentSimulatorBuilder {

  private final InputDataModelBuilder inputDataModelBuilder = new InputDataModelBuilder();
  private final InputDataDescriptionBuilder inputDataDescriptionBuilder = new InputDataDescriptionBuilder();

  public CloudManager build(WorkflowScheduleExperiment experiment) {
    String rootPath = experiment.getResultsDirectory() + File.separator + "workflowSchedule" + File.separator + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

    final CloudManagerParameters cloudManagerParameters = simulatorParameters(experiment.getSimulatorParameters(), rootPath);
    final InputDataModel inputDataModel = inputDataModelBuilder.build(experiment.getWorkflowDescription(), experiment.getInfrastructureDescription(), experiment.getDeadline());
    final InputDataDescription inputDataDescription = inputDataDescriptionBuilder.build(inputDataModel, experiment.getWorkflowDescription(), experiment.getInfrastructureDescription());

    return new CloudManager(cloudManagerParameters, inputDataModel, inputDataDescription, experiment.toString());
  }

  private CloudManagerParameters simulatorParameters(pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.SimulatorParameters input, String resultsDirectory) {
    final ExecutionTimeModifier executionTimeModifier = ExecutionTimeModifiers.getByName(input.getExecutionTimeModifier());
    final Boolean adjustToRealTime = input.isAdjustToRealTime();

    return new CloudManagerParameters(executionTimeModifier, adjustToRealTime, resultsDirectory);
  }

}
