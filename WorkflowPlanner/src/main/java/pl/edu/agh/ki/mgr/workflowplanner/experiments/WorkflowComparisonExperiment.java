package pl.edu.agh.ki.mgr.workflowplanner.experiments;

import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.SimulatorParameters;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

public class WorkflowComparisonExperiment {

  private SimulatorParameters simulatorParameters;
  private InfrastructureDescription infrastructureDescription;
  private WorkflowDescription firstWorkflowDescription;
  private WorkflowDescription secondWorkflowDescription;
  private WorkflowDescription thirdWorkflowDescription;

  private Long firstWorkflowDeadline;
  private Long secondWorkflowDeadline;
  private Long thirdWorkflowDeadline;

  private String resultsDirectory;

  public SimulatorParameters getSimulatorParameters() {
    return simulatorParameters;
  }

  public void setSimulatorParameters(SimulatorParameters simulatorParameters) {
    this.simulatorParameters = simulatorParameters;
  }

  public InfrastructureDescription getInfrastructureDescription() {
    return infrastructureDescription;
  }

  public void setInfrastructureDescription(InfrastructureDescription infrastructureDescription) {
    this.infrastructureDescription = infrastructureDescription;
  }

  public WorkflowDescription getFirstWorkflowDescription() {
    return firstWorkflowDescription;
  }

  public void setFirstWorkflowDescription(WorkflowDescription firstWorkflowDescription) {
    this.firstWorkflowDescription = firstWorkflowDescription;
  }

  public WorkflowDescription getSecondWorkflowDescription() {
    return secondWorkflowDescription;
  }

  public void setSecondWorkflowDescription(WorkflowDescription secondWorkflowDescription) {
    this.secondWorkflowDescription = secondWorkflowDescription;
  }

  public WorkflowDescription getThirdWorkflowDescription() {
    return thirdWorkflowDescription;
  }

  public void setThirdWorkflowDescription(WorkflowDescription thirdWorkflowDescription) {
    this.thirdWorkflowDescription = thirdWorkflowDescription;
  }

  public Long getFirstWorkflowDeadline() {
    return firstWorkflowDeadline;
  }

  public void setFirstWorkflowDeadline(Long firstWorkflowDeadline) {
    this.firstWorkflowDeadline = firstWorkflowDeadline;
  }

  public String getResultsDirectory() {
    return resultsDirectory;
  }

  public void setResultsDirectory(String resultsDirectory) {
    this.resultsDirectory = resultsDirectory;
  }

  public Long getSecondWorkflowDeadline() {
    return secondWorkflowDeadline;
  }

  public void setSecondWorkflowDeadline(Long secondWorkflowDeadline) {
    this.secondWorkflowDeadline = secondWorkflowDeadline;
  }

  public Long getThirdWorkflowDeadline() {
    return thirdWorkflowDeadline;
  }

  public void setThirdWorkflowDeadline(Long thirdWorkflowDeadline) {
    this.thirdWorkflowDeadline = thirdWorkflowDeadline;
  }

  @Override
  public String toString() {
    return "WorkflowComparisonExperiment{" +
        "simulatorParameters=" + simulatorParameters +
        ", infrastructureDescription=" + infrastructureDescription +
        ", firstWorkflowDescription=" + firstWorkflowDescription +
        ", secondWorkflowDescription=" + secondWorkflowDescription +
        ", thirdWorkflowDescription=" + thirdWorkflowDescription +
        ", firstWorkflowDeadline=" + firstWorkflowDeadline +
        ", secondWorkflowDeadline=" + secondWorkflowDeadline +
        ", thirdWorkflowDeadline=" + thirdWorkflowDeadline +
        ", resultsDirectory='" + resultsDirectory + '\'' +
        '}';
  }
}
