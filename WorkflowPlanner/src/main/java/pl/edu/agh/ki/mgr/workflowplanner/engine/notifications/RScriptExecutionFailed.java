package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class RScriptExecutionFailed extends AbstractSimulationNotification {
  private final Exception exception;

  public RScriptExecutionFailed(Exception exception) {
    super(NotificationType.RSCRIPT_EXECUTION_FAILED);
    this.exception = exception;
  }

  public Exception getException() {
    return exception;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". " + exception.toString();
  }
}
