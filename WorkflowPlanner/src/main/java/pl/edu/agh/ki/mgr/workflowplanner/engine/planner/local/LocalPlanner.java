package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local;

import jCMPL.*;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplSolutionSaver;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplConstants.*;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult.succeedResult;

public class LocalPlanner {

  public LevelAssignmentResult planLevel(LocalPlannerInputModel localPlannerInputModel, String pathToSolution) {
    try {
      return plan(localPlannerInputModel, pathToSolution);
    } catch (CmplException e) {
      Logger.LOGGER.error("Error in local planning phase.", e);
      return LevelAssignmentResult.failureResult(e);
    }
  }

  private LevelAssignmentResult plan(LocalPlannerInputModel localPlannerInputModel, String pathToSolution) throws CmplException {
    Level level = localPlannerInputModel.getLevel();
    List<Task> tasks = level.getTasks();
    List<Vm> vms = new ArrayList<>(localPlannerInputModel.getAssignedVms().keySet());

    Cmpl cmpl = new Cmpl(CMLP_LEVEL_MODEL_PATH);

    CmplSet vmsSet = new CmplSet(CMPL_V_VMS);
    vmsSet.setValues(1, vms.size());

    CmplSet tasksSet = new CmplSet(CMPL_K_TASKS);
    tasksSet.setValues(1, level.getTaskCount());

    CmplParameter taskOnVmTimeParameter = new CmplParameter(CMPL_Te_TIMES, tasksSet, vmsSet);
    long[][] taskTime = new long[tasks.size()][vms.size()];

    CmplParameter tasksNumberOnVmParameter = new CmplParameter(CMPL_N_TASK_NUM_ON_VM, vmsSet);
    long[] taskNumberOnVm = new long[vms.size()];

    for (int vm_idx = 0; vm_idx < vms.size(); vm_idx++) {
      Vm vm = vms.get(vm_idx);
      taskNumberOnVm[vm_idx] = localPlannerInputModel.getAssignedVms().get(vm);

      long vmCcu = vm.getCcu();
      for (int task_idx = 0; task_idx < tasks.size(); task_idx++) {
        Task task = tasks.get(task_idx);

        long computationTime = (long) Math.ceil((double) task.getEstimatedResource() / vmCcu);

        taskTime[task_idx][vm_idx] = computationTime;
      }
    }

    tasksNumberOnVmParameter.setValues(taskNumberOnVm);
    taskOnVmTimeParameter.setValues(taskTime);

    cmpl.setSets(vmsSet, tasksSet);
    cmpl.setParameters(taskOnVmTimeParameter, tasksNumberOnVmParameter);

    cmpl.solve();

    CmplSolutionSaver.save(pathToSolution, cmpl);

    return createResponse(localPlannerInputModel, cmpl, vms);
  }

  private LevelAssignmentResult createResponse(LocalPlannerInputModel localPlannerInputModel, Cmpl cmpl, List<Vm> vms) throws NumberFormatException, CmplException {
    LevelAssignmentResult response = succeedResult(localPlannerInputModel.getLevel());

    Pattern assignmentsPattern = Pattern.compile(CMPL_A_ASSIGNMENTS_VM_TO_TASK + "\\[(?<idxi>\\d+),(?<idxj>\\d+)\\]");

    for (CmplSolElement v : cmpl.solution().variables()) {
      String name = v.name();
      String activity = v.activity().toString();

      if ("1".equals(activity)) {
        Matcher taskAssignedMatcher = assignmentsPattern.matcher(name);
        if (taskAssignedMatcher.matches()) {
          int task_idx = Integer.parseInt(taskAssignedMatcher.group("idxi")) - 1;
          int vm_idx = Integer.parseInt(taskAssignedMatcher.group("idxj")) - 1;

          Task task = localPlannerInputModel.getLevel().getTasks().get(task_idx);
          Vm vm = vms.get(vm_idx);
          response.setAssignment(task, vm);
        }
      }
    }

    return response;
  }
}
