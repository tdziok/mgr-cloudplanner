package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model;

public enum GlobalPlanningModel {

  MAIN_MINIMIZE_COST_UNDER_DEADLINE("Main model (minimize total cost under given deadline)"),
  ALTERNATIVE_MINIMIZE_TIME("Alternative model (minimize time, deadline can not be meet)");

  private final String description;

  GlobalPlanningModel(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}

