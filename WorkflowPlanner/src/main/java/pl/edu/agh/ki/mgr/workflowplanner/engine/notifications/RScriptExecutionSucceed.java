package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class RScriptExecutionSucceed extends AbstractSimulationNotification {
  private final String scriptName;

  public RScriptExecutionSucceed(String scriptName) {
    super(NotificationType.RSCRIPT_EXECUTION_SUCCEED);
    this.scriptName = scriptName;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Script name: " + scriptName;
  }
}
