package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.GLOBAL_PLANNING_STARTED;

public class GlobalPlanningStartedNotification extends AbstractLevelAwareNotification {

  public GlobalPlanningStartedNotification(int level) {
    super(GLOBAL_PLANNING_STARTED, level);
  }

}
