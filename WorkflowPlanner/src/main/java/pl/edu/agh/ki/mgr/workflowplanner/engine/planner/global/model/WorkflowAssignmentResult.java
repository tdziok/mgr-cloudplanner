package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils.calculateComputationCost;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils.calculateComputationTime;

public class WorkflowAssignmentResult {

  private final GlobalPlanningModel planningModel;
  private final boolean successful;
  private final Exception exception;
  private Map<Level, List<VmWrapper>> levelToVmsMap = new HashMap<>();
  private Map<Level, Long> levelToTimeMap = new HashMap<>();

  private WorkflowAssignmentResult(GlobalPlanningModel planningModel, boolean successful, Exception exception) {
    this.planningModel = planningModel;
    this.successful = successful;
    this.exception = exception;
  }

  public static WorkflowAssignmentResult successfulResult(GlobalPlanningModel planningModel) {
    return new WorkflowAssignmentResult(planningModel, true, null);
  }

  public static WorkflowAssignmentResult failureResult(GlobalPlanningModel planningModel, Exception exception) {
    return new WorkflowAssignmentResult(planningModel, false, exception);
  }

  public Exception getException() {
    return exception;
  }

  public double getCost() {
    final double calculateCost = calculateCost();
    return calculateCost;
  }

  private double calculateCost() {
    return levelToVmsMap.entrySet()
        .stream()
        .collect(Collectors.summarizingDouble(this::toLevelCost))
        .getSum();
  }

  private double toLevelCost(Map.Entry<Level, List<VmWrapper>> entry) {
    final Level level = entry.getKey();
    final Long averageCcu = level.getAverageRequiredCcu();

    final List<VmWrapper> vmWrappers = entry.getValue();
    return vmWrappers.stream()
        .collect(Collectors.summarizingDouble(vmWrapper -> costForVm(vmWrapper, averageCcu)))
        .getSum();
  }

  private double costForVm(VmWrapper vmWrapper, Long taskCcu) {
    final Long tasksNumber = vmWrapper.getAssignedTasksNumber();
    final Vm vm = vmWrapper.getVm();
    final long vmCcu = vm.getCcu();
    final long pricePerHour = vm.getPricePerHour();

    final long computationTime = calculateComputationTime(taskCcu, vmCcu);
    final long computationCost = calculateComputationCost(computationTime, pricePerHour);
    final double totalCost = computationCost * tasksNumber;

    return totalCost;
  }

  public GlobalPlanningModel getPlanningModel() {
    return planningModel;
  }

  public WorkflowAssignmentResult addAssignment(Level level, Vm vm, long numberOfTasks) {
    List<VmWrapper> vmWrappers;
    if (levelToVmsMap.containsKey(level)) {
      vmWrappers = levelToVmsMap.get(level);
    } else {
      vmWrappers = new ArrayList<>();
    }
    vmWrappers.add(new VmWrapper(vm, numberOfTasks));
    levelToVmsMap.put(level, vmWrappers);
    return this;
  }

  public Map<Level, List<VmWrapper>> getLevelToVmsMap() {
    return levelToVmsMap;
  }

  public WorkflowAssignmentResult withTimeForLevel(Level assignedLevel, Long levelTime) {
    levelToTimeMap.put(assignedLevel, levelTime);
    return this;
  }

  public Long getLevelTime(Level level) {
    return levelToTimeMap.get(level);
  }

  public boolean failed() {
    return !successful;
  }

  public static class VmWrapper {
    private Vm vm;
    private Long assignedTasksNumber;

    public VmWrapper(Vm vm, Long assignedTasksNumber) {
      this.vm = vm;
      this.assignedTasksNumber = assignedTasksNumber;
    }

    public Vm getVm() {
      return vm;
    }

    public Long getAssignedTasksNumber() {
      return assignedTasksNumber;
    }

  }

}
