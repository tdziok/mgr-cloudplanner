package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl;

public class CmplConstants {

  public static final String CMLP_WORKFLOW_MODEL_MIN_COST_PATH = "models/workflowMinCost/workflowModel.cmpl";
  public static final String CMLP_WORKFLOW_MODEL_MIN_TIME_PATH = "models/workflowMinTime/workflowModel.cmpl";
  public static final String CMLP_LEVEL_MODEL_PATH = "models/level/levelModel.cmpl";
  public static final String CMPL_V_VMS = "V";
  public static final String CMPL_L_LEVELS = "L";
  public static final String CMPL_D_GLOBAL_DEADLINE = "d";
  public static final String CMPL_L_l_GROUPS_COUNT = "L_l";
  public static final String CMPL_C_COST = "C";
  public static final String CMPL_Ta_TIME = "Ta";
  public static final String CMPL_Q_NUMBER_OF_TASKS_ON_VM = "Q";
  public static final String CMPL_Te_EXECUTION_TIME_FOR_LEVEL = "Te";
  public static final String CMPL_Te_TIMES = "Te";
  public static final String CMPL_K_TASKS = "K";
  public static final String CMPL_N_TASK_NUM_ON_VM = "N";
  public static final String CMPL_A_ASSIGNMENTS_VM_TO_TASK = "A";

}
