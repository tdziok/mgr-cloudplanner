package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

public class NoRandomExecutionTime implements ExecutionTimeModifier {

  @Override
  public double getNextRandom() {
    return 1.0;
  }

  @Override
  public String getName() {
    return "NoRandom";
  }

}
