package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.LOCAL_PLANNING_SUCCEED;

public class LocalPlanningFailedNotification extends AbstractLevelAwareNotification {

  private final Exception exception;

  public LocalPlanningFailedNotification(int level, Exception exception) {
    super(LOCAL_PLANNING_SUCCEED, level);
    this.exception = exception;
  }

  public Exception getException() {
    return exception;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Error: " + exception.toString();
  }

}
