package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.EXECUTING_TASKS_FINISHED;

public class ExecutingTasksFinishedNotification extends AbstractLevelAwareNotification {
  public ExecutingTasksFinishedNotification(int level) {
    super(EXECUTING_TASKS_FINISHED, level);
  }
}
