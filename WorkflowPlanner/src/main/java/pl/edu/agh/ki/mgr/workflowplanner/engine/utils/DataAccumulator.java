package pl.edu.agh.ki.mgr.workflowplanner.engine.utils;

public class DataAccumulator {

  private Long currentValue;

  public DataAccumulator() {
    currentValue = 0L;
  }

  public Long getCurrentValue() {
    return currentValue;
  }

  public void add(Long value) {
    this.currentValue += value;
  }

}
