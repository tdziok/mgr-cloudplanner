package pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Infrastructure;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Workflow;
import pl.edu.agh.ki.mgr.workflowplanner.engine.Log4jObserver;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.WorkflowExecutor;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataDescription;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.*;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.GlobalPhasePlanner;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.GlobalPlannerWithDeadline;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.GlobalPlannerWithoutDeadline;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.LocalPlanner;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.ResultsFilesPaths;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv.CsvWriter;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ExecutionResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.IterationResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsToDisplayFactory;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.plots.RPlotGenerator;
import pl.edu.agh.ki.mgr.workflowplanner.utils.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;
import static pl.edu.agh.ki.mgr.workflowplanner.utils.LoggerFactory.FILE_LOGGER;

public class CloudManager implements PlotGenerationObserver {

  private final CloudManagerModelConverter cloudManagerModelConverter = new CloudManagerModelConverter();
  private final GlobalPhasePlanner globalPlannerWithDeadline = new GlobalPlannerWithDeadline();
  private final GlobalPhasePlanner globalPlannerWithoutDeadline = new GlobalPlannerWithoutDeadline();
  private final LocalPlanner localPlanner = new LocalPlanner();

  private final ResultsToDisplayFactory resultsToDisplayFactory = new ResultsToDisplayFactory();
  private final WorkflowExecutor workflowExecutor = new WorkflowExecutor();
  private final ResultsCollector resultsCollector = new ResultsCollector();
  private final InputDataModel inputDataModel;
  private final CloudManagerParameters cloudManagerParameters;
  private final CsvWriter csvWriter;
  private final RPlotGenerator RPlotGenerator;
  private final ResultsFilesPaths resultsFilesPaths;
  private final List<SimulationObserver> flowExecutionObservers = new LinkedList<>();

  public CloudManager(CloudManagerParameters cloudManagerParameters, InputDataModel inputDataModel, InputDataDescription inputDataDescription, String experimentDescription) {
    this.cloudManagerParameters = cloudManagerParameters;
    this.inputDataModel = inputDataModel;

    final String experimentName = inputDataDescription.experimentName();
    this.resultsFilesPaths = new ResultsFilesPaths(cloudManagerParameters.getResultsRootDirectory(), experimentName);

    FileAppender fileAppender = LoggerFactory.fileAppender(resultsFilesPaths.loggerPath());

    Logger.getRootLogger().addAppender(fileAppender);

    LOGGER.info("Executing experiment: {}", experimentDescription);

    this.csvWriter = new CsvWriter(resultsFilesPaths);
    this.RPlotGenerator = new RPlotGenerator(resultsFilesPaths);

    withSimulationObserver(new Log4jObserver());
  }

  public CloudManager withSimulationObserver(SimulationObserver observer) {
    flowExecutionObservers.add(observer);
    return this;
  }

  public ResultsCollector simulate() {
    List<Level> inputLevels = inputDataModel.getLevels();
    List<Level> levelsToPlan = new ArrayList<>(inputLevels);
    Long currentGlobalDeadline = inputDataModel.getGlobalDeadline();
    List<Vm> vms = inputDataModel.getVms();

    notifyObservers(new SimulationStartedNotification(inputDataModel));

    resultsCollector.setGivenDeadline(inputDataModel.getGlobalDeadline());

    for (int level_idx = 0; level_idx < inputLevels.size(); level_idx++) {
      final int currentLevelNumber = level_idx;
      notifyObservers(new IterationStartedNotification(currentLevelNumber));

      Level currentLevel = inputLevels.get(level_idx);
      final Infrastructure updatedInfrastructure = new Infrastructure(vms);
      final Workflow updatedWorkflow = new Workflow(levelsToPlan);
      InputDataModel currentInputData = new InputDataModel(currentGlobalDeadline, updatedInfrastructure, updatedWorkflow);

      notifyObservers(new GlobalPlanningStartedNotification(currentLevelNumber));
      WorkflowGlobalPlannerInputModel workflowGlobalPlannerInputModel = cloudManagerModelConverter.toWorkflowGlobalPlannerInput(currentInputData);
      WorkflowAssignmentResult planWorkflowResults = globalPlannerWithDeadline.planWorkflow(workflowGlobalPlannerInputModel, resultsFilesPaths.cmplGlobalMain(currentLevelNumber));

      if (planWorkflowResults.failed()) {
        notifyObservers(new GlobalPlanningFailedNotification(currentLevelNumber, planWorkflowResults.getException()));
        planWorkflowResults = globalPlannerWithoutDeadline.planWorkflow(workflowGlobalPlannerInputModel, resultsFilesPaths.cmplGlobalAlternative(currentLevelNumber));
        if (planWorkflowResults.failed()) {
          notifyObservers(new GlobalPlanningAlternativeFailedNotification(currentLevelNumber, planWorkflowResults.getException()));
        } else {
          notifyObservers(new GlobalPlanningAlternativeSucceedNotification(currentLevelNumber));
        }
      } else {
        notifyObservers(new GlobalPlanningSucceedNotification(currentLevelNumber));
      }

      notifyObservers(new LocalPlanningStartedNotification(currentLevelNumber));
      LocalPlannerInputModel localPlannerInputModel = cloudManagerModelConverter.toWorkflowLocalPlannerInput(currentLevel, planWorkflowResults);
      LevelAssignmentResult levelAssignmentResult = localPlanner.planLevel(localPlannerInputModel, resultsFilesPaths.cmplLocal(currentLevelNumber));
      if (levelAssignmentResult.isSucceed()) {
        notifyObservers(new LocalPlanningSucceedNotification(currentLevelNumber));
      } else {
        notifyObservers(new LocalPlanningFailedNotification(currentLevelNumber, levelAssignmentResult.getException()));
      }

      notifyObservers(new ExecutingTasksStartedNotification(currentLevelNumber));
      ExecutionResults taskExecutionResults = workflowExecutor.executeTasks(levelAssignmentResult, cloudManagerParameters.getExecutionTimeModifier());
      notifyObservers(new ExecutingTasksFinishedNotification(currentLevelNumber));

      IterationResults iterationResults = new IterationResults();
      iterationResults.setRemainingTimeBeforeScheduling(currentGlobalDeadline);
      iterationResults.setIterationId(level_idx);
      iterationResults.setWorkflowAssignmentResultsToDisplay(resultsToDisplayFactory.buildWorkflowAssignmentResults(planWorkflowResults));
      iterationResults.setLevelAssignmentResultsToDisplay(resultsToDisplayFactory.buildLevelAssignmentResults(levelAssignmentResult));
      iterationResults.setLevelExecutionResultsToDisplay(resultsToDisplayFactory.buildLevelExecutionResults(taskExecutionResults));
      resultsCollector.addResult(iterationResults);

      notifyObservers(new IterationFinishedNotification(currentLevelNumber, iterationResults, resultsCollector));

      int size = levelsToPlan.size();
      levelsToPlan = new ArrayList<>(levelsToPlan.subList(1, size));

      if (cloudManagerParameters.shouldTakeIntoAccountRealExecutionTime()) {
        currentGlobalDeadline -= taskExecutionResults.getRealExecutionTime();
      } else {
        currentGlobalDeadline -= resultsToDisplayFactory.buildLevelAssignmentResults(levelAssignmentResult).getEstimatedTime();
      }
    }

    notifyObservers(new SimulationFinishedNotification());

    notifyObservers(new GenerationPlotStartedNotification());

    RPlotGenerator.addObserver(this);
    csvWriter.writeResultsForTimePlot(resultsCollector);
    RPlotGenerator.generateScheduleTimePlot(resultsCollector.getGivenDeadline(), resultsCollector.getMaxYValueForTime());

    csvWriter.writeResultsForCostPlot(resultsCollector);
    RPlotGenerator.generateScheduleCostPlot((long) (resultsCollector.getCurrentRealCost() * 1.2));

    Logger.getRootLogger().removeAppender(FILE_LOGGER);
    return resultsCollector;
  }

  private void notifyObservers(SimulationProgressNotification notification) {
    for (SimulationObserver simulationObserver : flowExecutionObservers) {
      simulationObserver.onProgressUpdate(notification);
    }
  }

  @Override
  public void onUpdate(SimulationProgressNotification notification) {
    notifyObservers(notification);
  }
}
