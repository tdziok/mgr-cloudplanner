package pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv;

import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.IterationResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.LevelEstimatedResultsToDisplay;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class CsvTimePlotDataProducer {
  public List<List<Object>> produceData(ResultsCollector resultsCollector) {
    List<List<Object>> records = new ArrayList<>();

    List<IterationResults> results = resultsCollector.getResults();
    int levelsNumber = results.size();

    long sumAddedLevelEstimateTime = 0;
    long sumAddedLevelRealTime = 0;

    long iterationCounter = 0;
    for (IterationResults result : results) {
      final List<Object> record = new ArrayList<>();
      record.add(1 + iterationCounter++);
      record.add(sumAddedLevelRealTime);

      Long levelId = result.getLevelExecutionResultsToDisplay().getLevelId();
      List<LevelEstimatedResultsToDisplay> levelsResult = result.getWorkflowAssignmentResultsToDisplay().getLevels();

      for (long tmp = 0; tmp < levelId; tmp++) {
        record.add(0L);
      }

      for (LevelEstimatedResultsToDisplay levelEstimation : levelsResult) {
        record.add(levelEstimation.getLevelTime());
      }

      long executionTimeForCurrentLevel = result.getLevelExecutionResultsToDisplay().getExecutionTime();
      long estimatedTimeForCurrentLevel = result.getLevelAssignmentResultsToDisplay().getEstimatedTime();

      sumAddedLevelEstimateTime = sumAddedLevelRealTime + estimatedTimeForCurrentLevel;
      sumAddedLevelRealTime += executionTimeForCurrentLevel;

      record.add(sumAddedLevelEstimateTime);
      records.add(record);

    }
    List<Object> lastRecord = new ArrayList<>();
    lastRecord.add(1 + iterationCounter++);
    lastRecord.add(sumAddedLevelRealTime);
    IntStream.rangeClosed(1, levelsNumber).forEach(ignored -> lastRecord.add(0L));

    lastRecord.add(sumAddedLevelRealTime);
    records.add(lastRecord);

    return records;
  }

  public List<String> produceHeaders(ResultsCollector resultsCollector) {
    List<IterationResults> results = resultsCollector.getResults();
    int levelsNumber = results.size();

    List<String> headers = new ArrayList<>();
    headers.add("Iteration");
    headers.add("Completed");
    IntStream.rangeClosed(1, levelsNumber).forEach(id -> headers.add("Level " + id));
    headers.add("PlanNext");

    return headers;
  }
}
