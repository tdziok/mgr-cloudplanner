package pl.edu.agh.ki.mgr.workflowplanner.engine.results.plots;

import org.apache.commons.exec.*;
import pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.*;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.ResultsFilesPaths;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class RPlotGenerator {

  public static final RScriptsPlotsPaths R_SCRIPTS_PLOTS_PATHS = new RScriptsPlotsPaths();
  private static final String RSCRIPT_EXE = "Rscript.exe";

  private final List<PlotGenerationObserver> observers = new LinkedList<>();
  private final ResultsFilesPaths resultsFilesPaths;

  public RPlotGenerator(ResultsFilesPaths resultsFilesPaths) {
    this.resultsFilesPaths = resultsFilesPaths;
  }

  public void addObserver(PlotGenerationObserver observer) {
    observers.add(observer);
  }

  public void generateScheduleTimePlot(long globalDeadline, long maxYValue) {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.scheduleTimePlot(),
        String.valueOf(globalDeadline),
        String.valueOf(maxYValue),
        resultsFilesPaths.csvWithWorkflowTime(),
        resultsFilesPaths.plotWithWorkflowTime());
  }

  public void generateScheduleCostPlot(long maxYValue) {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.scheduleCostPlot(),
        null,
        String.valueOf(maxYValue),
        resultsFilesPaths.csvWithWorkflowCost(),
        resultsFilesPaths.plotWithWorkflowCost());
  }

  public void generateDisturbanceTimePlot(Long globalDeadline) {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.disturbanceTimePlot(),
        globalDeadline.toString(),
        null,
        resultsFilesPaths.csvWithWorkflowDisturbanceTime(),
        resultsFilesPaths.plotWithWorkflowDisturbanceTime());

  }

  public void generateDisturbanceCostPlot() {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.disturbanceCostPlot(),
        null,
        null,
        resultsFilesPaths.csvWithWorkflowDisturbanceCost(),
        resultsFilesPaths.plotWithWorkflowDisturbanceCost());
  }

  public void generateWorkflowComparisonTimePlot() {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.comparisonTimePlot(),
        null,
        null,
        resultsFilesPaths.csvWithWorkflowComparisonTime(),
        resultsFilesPaths.plotWithWorkflowComparisonTime());
  }

  public void generateWorkflowComparisonCostPlot() {
    executeRScript(R_SCRIPTS_PLOTS_PATHS.comparisonCostPlot(),
        null,
        null,
        resultsFilesPaths.csvWithWorkflowComparisonCost(),
        resultsFilesPaths.plotWithWorkflowComparisonCost());
  }

  private void executeRScript(String scriptPath,
                              String globalDeadline,
                              String maxYValue,
                              String pathToInputData,
                              String pathToOutputData) {

    new File(pathToOutputData).getParentFile().mkdirs();
    CommandLine cmdLine = new CommandLine(RSCRIPT_EXE);
    cmdLine.addArgument(scriptPath);
    cmdLine.addArguments(pathToInputData);
    cmdLine.addArguments(pathToOutputData);
    if (globalDeadline != null) {
      cmdLine.addArguments(globalDeadline);
    }
    if (maxYValue != null) {
      cmdLine.addArgument(maxYValue);
    }

    DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

    ExecuteWatchdog watchdog = new ExecuteWatchdog(60 * 1000);
    Executor executor = new DefaultExecutor();
    executor.setExitValue(0);
    executor.setWatchdog(watchdog);
    try {
      notifyObservers(new RScriptExecutionStarted(cmdLine.toString()));
      executor.execute(cmdLine, resultHandler);
      resultHandler.waitFor();
      notifyObservers(new RScriptExecutionSucceed(scriptPath));
    } catch (Exception e) {
      LOGGER.error("Error when generating plot", e);
      notifyObservers(new RScriptExecutionFailed(e));
    }

    final int exitValue = resultHandler.getExitValue();
    if (exitValue != 0) {
      final ExecuteException executeException = resultHandler.getException();
      LOGGER.error("Error when saving cmpl solution", executeException);
      notifyObservers(new RScriptExecutionFailed(executeException));
    }

  }

  private void notifyObservers(SimulationProgressNotification notification) {
    for (PlotGenerationObserver observer : observers) {
      observer.onUpdate(notification);
    }
  }

}
