package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl;

import jCMPL.Cmpl;
import jCMPL.CmplException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class CmplSolutionSaver {
  public static void save(String pathToSolution, Cmpl cmpl) throws CmplException {

    if (StringUtils.isBlank(pathToSolution)) {
      return;
    }

    final Path path1 = Paths.get(pathToSolution).getParent();
    try {
      Files.createDirectories(path1);
      cmpl.saveSolutionCsv(pathToSolution);
    } catch (IOException e) {
      LOGGER.error("Error when saving cmpl solution", e);
    }
  }
}
