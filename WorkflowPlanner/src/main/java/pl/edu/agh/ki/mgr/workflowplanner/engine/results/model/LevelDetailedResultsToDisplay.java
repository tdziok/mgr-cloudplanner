package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class LevelDetailedResultsToDisplay {

  private long taskId;
  private long taskCcu;
  private long vmId;
  private long vmCcu;
  private long estimatedTime;
  private long estimatedCost;

  public long getTaskId() {
    return taskId;
  }

  public void setTaskId(long taskId) {
    this.taskId = taskId;
  }

  public long getTaskCcu() {
    return taskCcu;
  }

  public void setTaskCcu(long taskCcu) {
    this.taskCcu = taskCcu;
  }

  public long getVmId() {
    return vmId;
  }

  public void setVmId(long vmId) {
    this.vmId = vmId;
  }

  public long getVmCcu() {
    return vmCcu;
  }

  public void setVmCcu(long vmCcu) {
    this.vmCcu = vmCcu;
  }

  public long getEstimatedTime() {
    return estimatedTime;
  }

  public void setEstimatedTime(long estimatedTime) {
    this.estimatedTime = estimatedTime;
  }

  public long getEstimatedCost() {
    return estimatedCost;
  }

  public void setEstimatedCost(long estimatedCost) {
    this.estimatedCost = estimatedCost;
  }

}
