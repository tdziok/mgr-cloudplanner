package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class IterationResults {

  private long iterationId;
  private long remainingTimeBeforeScheduling;

  private WorkflowAssignmentResultsToDisplay workflowAssignmentResultsToDisplay;
  private LevelAssignmentResultsToDisplay levelAssignmentResultsToDisplay;
  private LevelExecutionResultsToDisplay levelExecutionResultsToDisplay;

  public long getIterationId() {
    return iterationId;
  }

  public void setIterationId(long iterationId) {
    this.iterationId = iterationId;
  }

  public long getRemainingTimeBeforeScheduling() {
    return remainingTimeBeforeScheduling;
  }

  public void setRemainingTimeBeforeScheduling(long remainingTimeBeforeScheduling) {
    this.remainingTimeBeforeScheduling = remainingTimeBeforeScheduling;
  }

  public WorkflowAssignmentResultsToDisplay getWorkflowAssignmentResultsToDisplay() {
    return workflowAssignmentResultsToDisplay;
  }

  public void setWorkflowAssignmentResultsToDisplay(WorkflowAssignmentResultsToDisplay workflowAssignmentResultsToDisplay) {
    this.workflowAssignmentResultsToDisplay = workflowAssignmentResultsToDisplay;
  }

  public LevelAssignmentResultsToDisplay getLevelAssignmentResultsToDisplay() {
    return levelAssignmentResultsToDisplay;
  }

  public void setLevelAssignmentResultsToDisplay(LevelAssignmentResultsToDisplay levelAssignmentResultsToDisplay) {
    this.levelAssignmentResultsToDisplay = levelAssignmentResultsToDisplay;
  }

  public LevelExecutionResultsToDisplay getLevelExecutionResultsToDisplay() {
    return levelExecutionResultsToDisplay;
  }

  public void setLevelExecutionResultsToDisplay(LevelExecutionResultsToDisplay levelExecutionResultsToDisplay) {
    this.levelExecutionResultsToDisplay = levelExecutionResultsToDisplay;
  }

}
