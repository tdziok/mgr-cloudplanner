package pl.edu.agh.ki.mgr.workflowplanner.engine.results.plots;

import java.io.File;

class RScriptsPlotsPaths {

  private static final String SEPARATOR = File.separator;

  private static final String RSCRIPTS = "RScripts";

  private static final String R_WORKFLOW_SCHEDULE_TIME_PLOT_R = "workflowScheduleTimePlot.R";
  private static final String R_WORKFLOW_SCHEDULE_COST_PLOT_R = "workflowScheduleCostPlot.R";
  private static final String R_WORKFLOW_DISTURBANCE_TIME_PLOT_R = "workflowDisturbanceTimePlot.R";
  private static final String R_WORKFLOW_DISTURBANCE_COST_PLOT_R = "workflowDisturbanceCostPlot.R";
  private static final String R_WORKFLOW_COMPARISON_TIME_PLOT_R = "workflowComparisonTimePlot.R";
  private static final String R_WORKFLOW_COMPARISON_COST_PLOT_R = "workflowComparisonCostPlot.R";

  public String comparisonTimePlot() {
    return plot(R_WORKFLOW_COMPARISON_TIME_PLOT_R);
  }

  public String comparisonCostPlot() {
    return plot(R_WORKFLOW_COMPARISON_COST_PLOT_R);
  }

  public String disturbanceCostPlot() {
    return plot(R_WORKFLOW_DISTURBANCE_COST_PLOT_R);
  }

  public String disturbanceTimePlot() {
    return plot(R_WORKFLOW_DISTURBANCE_TIME_PLOT_R);
  }

  public String scheduleCostPlot() {
    return plot(R_WORKFLOW_SCHEDULE_COST_PLOT_R);
  }

  public String scheduleTimePlot() {
    return plot(R_WORKFLOW_SCHEDULE_TIME_PLOT_R);
  }

  private String plot(String filename) {
    return new StringBuilder()
        .append(RSCRIPTS)
        .append(SEPARATOR)
        .append(filename)
        .toString();
  }
}
