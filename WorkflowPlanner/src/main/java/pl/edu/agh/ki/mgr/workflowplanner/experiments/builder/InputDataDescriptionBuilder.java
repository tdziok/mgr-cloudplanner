package pl.edu.agh.ki.mgr.workflowplanner.experiments.builder;

import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataDescription;
import pl.edu.agh.ki.mgr.workflowplanner.engine.input.InputDataModel;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

public class InputDataDescriptionBuilder {
  public InputDataDescription build(InputDataModel inputDataModel, WorkflowDescription workflowDescription, InfrastructureDescription infrastructureDescription) {
    String workflowName = workflowDescription.getName();
    String infrastructureName = infrastructureDescription.getName();
    InputDataDescription description = new InputDataDescription(inputDataModel, workflowName, infrastructureName);

    return description;
  }
}
