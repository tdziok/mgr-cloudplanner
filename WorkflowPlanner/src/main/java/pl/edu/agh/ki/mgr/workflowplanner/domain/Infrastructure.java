package pl.edu.agh.ki.mgr.workflowplanner.domain;

import java.util.List;

public class Infrastructure {

  private final List<Vm> vms;

  public Infrastructure(List<Vm> vms) {
    this.vms = vms;
  }

  public List<Vm> getVms() {
    return vms;
  }
}
