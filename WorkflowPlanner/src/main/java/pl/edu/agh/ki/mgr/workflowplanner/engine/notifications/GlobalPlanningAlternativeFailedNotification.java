package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class GlobalPlanningAlternativeFailedNotification extends AbstractLevelAwareNotification {

  private final Exception exception;

  public GlobalPlanningAlternativeFailedNotification(int currentLevelNumber, Exception exception) {
    super(NotificationType.GLOBAL_PLANNING_ALTERNATIVE_FAILED, currentLevelNumber);

    this.exception = exception;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Error: " + exception.toString();
  }
}
