package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

public interface ExecutionTimeModifier {

  double getNextRandom();

  default double getNextRandomForTask(long taskId) {
    return getNextRandom();
  }

  String getName();
}
