package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.IterationResults;
import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.ResultsCollector;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.ITERATION_FINISHED;

public class IterationFinishedNotification extends AbstractLevelAwareNotification {
  private final IterationResults iterationResults;
  private final ResultsCollector resultsCollector;

  public IterationFinishedNotification(int level, IterationResults iterationResults, ResultsCollector resultsCollector) {
    super(ITERATION_FINISHED, level);
    this.iterationResults = iterationResults;
    this.resultsCollector = resultsCollector;
  }

  public IterationResults getIterationResults() {
    return iterationResults;
  }

  public ResultsCollector getResultsCollector() {
    return resultsCollector;
  }
}
