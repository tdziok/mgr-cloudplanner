package pl.edu.agh.ki.mgr.workflowplanner.experiments;

import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;

public class WorkflowDisturbanceExperiment {

  private InfrastructureDescription infrastructureDescription;
  private WorkflowDescription workflowDescription;

  private Long deadline;

  private String resultsDirectory;

  public InfrastructureDescription getInfrastructureDescription() {
    return infrastructureDescription;
  }

  public void setInfrastructureDescription(InfrastructureDescription infrastructureDescription) {
    this.infrastructureDescription = infrastructureDescription;
  }

  public WorkflowDescription getWorkflowDescription() {
    return workflowDescription;
  }

  public void setWorkflowDescription(WorkflowDescription workflowDescription) {
    this.workflowDescription = workflowDescription;
  }

  public Long getDeadline() {
    return deadline;
  }

  public void setDeadline(Long deadline) {
    this.deadline = deadline;
  }

  public String getResultsDirectory() {
    return resultsDirectory;
  }

  public void setResultsDirectory(String resultsDirectory) {
    this.resultsDirectory = resultsDirectory;
  }

  @Override
  public String toString() {
    return "WorkflowDisturbanceExperiment{" +
        "infrastructureDescription=" + infrastructureDescription +
        ", workflowDescription=" + workflowDescription +
        ", deadline=" + deadline +
        ", resultsDirectory='" + resultsDirectory + '\'' +
        '}';
  }
}
