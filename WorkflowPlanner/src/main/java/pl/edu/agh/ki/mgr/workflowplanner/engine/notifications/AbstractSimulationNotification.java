package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

abstract class AbstractSimulationNotification implements SimulationProgressNotification {

  private final NotificationType notificationType;

  protected AbstractSimulationNotification(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  @Override
  public String getDescription() {
    return notificationType.getDescription();
  }

  @Override
  public NotificationType getType() {
    return notificationType;
  }
}
