package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.LOCAL_PLANNING_STARTED;

public class LocalPlanningStartedNotification extends AbstractLevelAwareNotification {
  public LocalPlanningStartedNotification(int level) {
    super(LOCAL_PLANNING_STARTED, level);
  }
}
