package pl.edu.agh.ki.mgr.workflowplanner.engine.input;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Infrastructure;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Workflow;

import java.util.List;

public class InputDataModel {

  private final Long globalDeadline;
  private final Infrastructure infrastructure;
  private final Workflow workflow;

  public InputDataModel(Long globalDeadline, Infrastructure infrastructure, Workflow workflow) {
    this.globalDeadline = globalDeadline;
    this.infrastructure = infrastructure;
    this.workflow = workflow;
  }

  public Long getGlobalDeadline() {
    return globalDeadline;
  }

  public List<Vm> getVms() {
    return infrastructure.getVms();
  }

  public List<Level> getLevels() {
    return workflow.getLevels();
  }

}
