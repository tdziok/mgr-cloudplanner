package pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model;

import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowComparisonExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowDisturbanceExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowScheduleExperiment;

import java.util.List;

import static java.util.Collections.emptyList;

public class ExperimentsInput {
  private List<WorkflowScheduleExperiment> workflowScheduleExperiments = emptyList();
  private List<WorkflowComparisonExperiment> workflowComparisonExperiments = emptyList();
  private List<WorkflowDisturbanceExperiment> workflowDisturbanceExperiments = emptyList();

  public List<WorkflowScheduleExperiment> getWorkflowScheduleExperiments() {
    return workflowScheduleExperiments;
  }

  public void setWorkflowScheduleExperiments(List<WorkflowScheduleExperiment> workflowScheduleExperiments) {
    this.workflowScheduleExperiments = workflowScheduleExperiments;
  }

  public List<WorkflowComparisonExperiment> getWorkflowComparisonExperiments() {
    return workflowComparisonExperiments;
  }

  public void setWorkflowComparisonExperiments(List<WorkflowComparisonExperiment> workflowComparsionExperiments) {
    this.workflowComparisonExperiments = workflowComparsionExperiments;
  }

  public List<WorkflowDisturbanceExperiment> getWorkflowDisturbanceExperiments() {
    return workflowDisturbanceExperiments;
  }

  public void setWorkflowDisturbanceExperiments(List<WorkflowDisturbanceExperiment> workflowDisturbanceExperiments) {
    this.workflowDisturbanceExperiments = workflowDisturbanceExperiments;
  }

  @Override
  public String toString() {
    return "ExperimentsInput{" +
        "\nworkflowScheduleExperiments=" + workflowScheduleExperiments +
        ", \nworkflowComparisonExperiments=" + workflowComparisonExperiments +
        ", \nworkflowDisturbanceExperiments=" + workflowDisturbanceExperiments +
        '}';
  }
}
