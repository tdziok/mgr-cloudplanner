package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public enum NotificationType {
  SIMULATION_STARTED("Simulation started"),
  SIMULATION_UPDATE("Simulation status updated"),
  SIMULATION_FINISHED("Simulation finished"),
  ITERATION_STARTED("Iteration started"),
  ITERATION_FINISHED("Iteration finished"),
  GLOBAL_PLANNING_STARTED("Global planning started"),
  GLOBAL_PLANNING_FAILED("Global planning failed"),
  GLOBAL_PLANNING_ALTERNATIVE_SUCCEED("Global planning alternative succeed"),
  GLOBAL_PLANNING_ALTERNATIVE_FAILED("Global planning alternative failed"),
  GLOBAL_PLANNING_SUCCEED("Global planning succeed"),
  LOCAL_PLANNING_STARTED("Local planning started"),
  LOCAL_PLANNING_SUCCEED("Local planning succeed"),
  EXECUTING_TASKS_STARTED("Executing tasks started"),
  EXECUTING_TASKS_FINISHED("Executing tasks finished"),
  PLOTS_GENERATION_STARTED("Started plots generation"),
  PLOTS_GENERATION_FINISHED("Finished plots generation"),
  RSCRIPT_EXECUTION_STARTED("Executing R script started"),
  RSCRIPT_EXECUTION_FAILED("Executing R script failed"),
  RSCRIPT_EXECUTION_SUCCEED("Executing R script succeed"),;

  private final String description;

  NotificationType(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
