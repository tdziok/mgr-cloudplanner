package pl.edu.agh.ki.mgr.workflowplanner.experiments.validation;

import org.apache.commons.lang3.StringUtils;
import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifiers;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowScheduleExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.ExperimentsInput;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.InfrastructureDescription;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.SimulatorParameters;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.WorkflowDescription;
import pl.edu.agh.ki.mgr.workflowplanner.utils.BadInputException;

import java.io.File;
import java.util.List;

public class ExperimentInputValidator {

  public void validate(ExperimentsInput experimentsInput) {
    if (experimentsInput == null) {
      breakAndThrowExceptionWhenMissing("configuration");
    }

    validate(experimentsInput.getWorkflowScheduleExperiments());
  }

  private void validate(List<WorkflowScheduleExperiment> workflowScheduleExperiments) {
    if (workflowScheduleExperiments == null) {
      breakAndThrowExceptionWhenMissing("at least one WorkflowScheduleExperiment should be present");
    }

    workflowScheduleExperiments.forEach(this::validate);
  }

  private void validate(WorkflowScheduleExperiment experiment) {
    validate(experiment.getSimulatorParameters());
    validate(experiment.getInfrastructureDescription());
    validate(experiment.getWorkflowDescription());
    validate(experiment.getDeadline());
    validateResultsDirectory(experiment.getResultsDirectory());
  }

  private void validateResultsDirectory(String resultsDirectory) {
    if (StringUtils.isBlank(resultsDirectory)) {
      breakAndThrowExceptionWhenMissing("resultsDirectory");
    }

    validateIfDirectoryExists(resultsDirectory);

  }

  private void validate(Long deadline) {
    if (deadline == null) {
      breakAndThrowExceptionWhenMissing("deadline");
    }

    if (deadline <= 0) {
      breakAndThrowExceptionWhenBadValue("deadline must be greater than 0. Current value: " + deadline);
    }
  }

  private void validate(WorkflowDescription workflowDescription) {
    if (workflowDescription == null) {
      breakAndThrowExceptionWhenMissing("workflowDescription");
    }
    String filePath = workflowDescription.getFilePath();
    if (StringUtils.isBlank(filePath)) {
      breakAndThrowExceptionWhenMissing("filePath");
    }

    validateIfFileExists(filePath);

    String name = workflowDescription.getName();
    if (StringUtils.isBlank(name)) {
      breakAndThrowExceptionWhenMissing("name");
    }
  }

  private void validate(InfrastructureDescription infrastructureDescription) {
    if (infrastructureDescription == null) {
      breakAndThrowExceptionWhenMissing("infrastructureDescription");
    }

    String filePath = infrastructureDescription.getFilePath();
    if (StringUtils.isBlank(filePath)) {
      breakAndThrowExceptionWhenMissing("filePath");
    }

    validateIfFileExists(filePath);

    String name = infrastructureDescription.getName();
    if (StringUtils.isBlank(name)) {
      breakAndThrowExceptionWhenMissing("name");
    }

  }

  private void validateIfFileExists(String filePath) {
    File file = new File(filePath);
    if (!file.exists() || !file.canRead() || !file.isFile()) {
      breakAndThrowExceptionWhenBadValue("unable to read file: " + filePath);
    }
  }

  private void validateIfDirectoryExists(String directoryPath) {
    File file = new File(directoryPath);
    if (!file.isDirectory() || !file.canRead()) {
      breakAndThrowExceptionWhenBadValue("unable to read directory: " + directoryPath);
    }
  }

  private void validate(SimulatorParameters simulatorParameters) {
    if (simulatorParameters == null) {
      breakAndThrowExceptionWhenMissing("simulatorParameters");
    }

    String executionTimeModifier = simulatorParameters.getExecutionTimeModifier();
    if (StringUtils.isBlank(executionTimeModifier)) {
      breakAndThrowExceptionWhenMissing("executionTimeModifier");
    }

    if (!ExecutionTimeModifiers.isSupported(executionTimeModifier)) {
      breakAndThrowExceptionWhenBadValue("executionTimeModifier is not supported: " + executionTimeModifier);
    }

    if (simulatorParameters.isAdjustToRealTime() == null) {
    }

  }

  private void breakAndThrowExceptionWhenMissing(String message) {
    throw new BadInputException("Missing element in configuration: " + message);
  }

  private void breakAndThrowExceptionWhenBadValue(String message) {
    throw new BadInputException("Bad element value in configuration: " + message);
  }
}
