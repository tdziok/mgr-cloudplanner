package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

import java.util.HashMap;
import java.util.Map;

public class ExecutionTimeModifiers {

  private static final Map<String, ExecutionTimeModifier> executionTimeModifiers = init();

  private static Map<String, ExecutionTimeModifier> init() {
    final HashMap<String, ExecutionTimeModifier> executionTimeModifiers = new HashMap<>();
    executionTimeModifiers.put("GaussianExecutionTimeModifier", new GaussianExecutionTimeModifier());
    executionTimeModifiers.put("GaussianLongerExecutionTime", new GaussianLongerExecutionTime());
    executionTimeModifiers.put("GaussianShorterExecutionTime", new GaussianShorterExecutionTime());
    executionTimeModifiers.put("NoRandomExecutionTime", new NoRandomExecutionTime());
    executionTimeModifiers.put("PaperExecutionTimeModifier", new PaperExecutionTimeModifier());
    return executionTimeModifiers;
  }

  public static boolean isSupported(String name) {
    return executionTimeModifiers.containsKey(name);
  }

  public static ExecutionTimeModifier getByName(String executionTimeModifier) {
    if (!executionTimeModifier.contains(executionTimeModifier)) {
      throw new IllegalStateException("unknown execution time modifier: " + executionTimeModifier);
    }

    return executionTimeModifiers.get(executionTimeModifier);
  }
}
