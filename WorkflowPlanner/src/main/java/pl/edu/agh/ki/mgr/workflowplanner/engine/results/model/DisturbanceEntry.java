package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class DisturbanceEntry {
  private double disturbance;

  private long adaptiveTime;
  private long staticTime;

  private long adaptiveCost;
  private long staticCost;


  public DisturbanceEntry withDisturbance(double disturbance) {
    this.disturbance = disturbance;
    return this;
  }

  public DisturbanceEntry withAdaptiveTime(long adaptiveTime) {
    this.adaptiveTime = adaptiveTime;
    return this;
  }

  public DisturbanceEntry withStaticTime(long staticTime) {
    this.staticTime = staticTime;
    return this;
  }

  public long getAdaptiveCost() {
    return adaptiveCost;
  }

  public DisturbanceEntry withAdaptiveCost(long adaptiveCost) {
    this.adaptiveCost = adaptiveCost;
    return this;
  }

  public long getStaticCost() {
    return staticCost;
  }

  public DisturbanceEntry withStaticCost(long staticCost) {
    this.staticCost = staticCost;
    return this;
  }

  public double getDisturbance() {
    return disturbance;
  }

  public long getAdaptiveTime() {
    return adaptiveTime;
  }

  public long getStaticTime() {
    return staticTime;
  }

  @Override
  public String toString() {
    return "DisturbanceEntry{" +
        "disturbance=" + disturbance +
        ", adaptiveTime=" + adaptiveTime +
        ", staticTime=" + staticTime +
        '}';
  }
}
