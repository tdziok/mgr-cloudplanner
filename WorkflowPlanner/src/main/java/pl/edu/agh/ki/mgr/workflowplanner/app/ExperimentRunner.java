package pl.edu.agh.ki.mgr.workflowplanner.app;

import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowComparisonExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowDisturbanceExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.WorkflowScheduleExperiment;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.ExperimentInputParser;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.input.model.ExperimentsInput;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.runners.WorkflowComparisonExperimentRunner;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.runners.WorkflowDisturbanceExperimentRunner;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.runners.WorkflowScheduleExperimentRunner;
import pl.edu.agh.ki.mgr.workflowplanner.experiments.validation.ExperimentInputValidator;
import pl.edu.agh.ki.mgr.workflowplanner.utils.BadInputException;

import static pl.edu.agh.ki.mgr.workflowplanner.utils.Logger.LOGGER;

public class ExperimentRunner {

  private final ExperimentInputParser inputParser = new ExperimentInputParser();
  private final ExperimentInputValidator inputValidator = new ExperimentInputValidator();
  private final WorkflowScheduleExperimentRunner workflowScheduleExperimentRunner = new WorkflowScheduleExperimentRunner();
  private final WorkflowComparisonExperimentRunner workflowComparisonExperimentRunner = new WorkflowComparisonExperimentRunner();
  private final WorkflowDisturbanceExperimentRunner workflowDisturbanceExperimentRunner = new WorkflowDisturbanceExperimentRunner();

  public static void main(String[] args) {
    if (args.length < 1) {
      System.out.println("Missing path to file with experiments config");
    }

    try {
      new ExperimentRunner().run(args[0]);
    } catch (BadInputException e) {
      LOGGER.error("Error when parsing input", e);
    }
  }

  private void run(String parametersFile) {
    LOGGER.info("----------------------------------------------------------");
    LOGGER.info("Reading experiments input from file: {} ", parametersFile);
    ExperimentsInput experimentsInput = inputParser.parse(parametersFile);

    LOGGER.info("Validating experiments input");
    inputValidator.validate(experimentsInput);

    LOGGER.info("Experiments number: {}", experimentsInput.getWorkflowScheduleExperiments().size());
    LOGGER.info("Read input: {}", experimentsInput);

    for (WorkflowScheduleExperiment workflowScheduleExperiment : experimentsInput.getWorkflowScheduleExperiments()) {
      workflowScheduleExperimentRunner.execute(workflowScheduleExperiment);
    }

    for (WorkflowComparisonExperiment workflowComparisonExperiment : experimentsInput.getWorkflowComparisonExperiments()) {
      workflowComparisonExperimentRunner.execute(workflowComparisonExperiment);
    }

    for (WorkflowDisturbanceExperiment workflowDisturbanceExperiment : experimentsInput.getWorkflowDisturbanceExperiments()) {
      workflowDisturbanceExperimentRunner.execute(workflowDisturbanceExperiment);
    }

  }

}
