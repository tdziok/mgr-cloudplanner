package pl.edu.agh.ki.mgr.workflowplanner.engine.results.csv;

import pl.edu.agh.ki.mgr.workflowplanner.engine.results.model.WorkflowThreeTimeExecResults;

import java.util.ArrayList;
import java.util.List;

public class CsvComparisonTimeDataProducer {
  public List<List<Object>> records(List<WorkflowThreeTimeExecResults> workflowThreeTimeExecResults) {
    List<List<Object>> records = new ArrayList<>();

    for (WorkflowThreeTimeExecResults e : workflowThreeTimeExecResults) {

      List<Object> entry = new ArrayList<>();
      entry.add(e.getWorkflowName());
      entry.add(e.getOverEstimatedExecutionTime());
      entry.add(e.getExactEstimatedExecutionTime());
      entry.add(e.getUnderEstimatedExecutionTime());
      records.add(entry);
    }

    return records;
  }

  public List<String> headers() {
    List<String> headers = new ArrayList<>();
    headers.add("Workflow");
    headers.add("Overestimation");
    headers.add("Exact estimation");
    headers.add("Underestimation");

    return headers;
  }
}
