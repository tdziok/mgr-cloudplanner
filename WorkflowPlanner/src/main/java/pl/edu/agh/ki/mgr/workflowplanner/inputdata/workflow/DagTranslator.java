package pl.edu.agh.ki.mgr.workflowplanner.inputdata.workflow;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.IdGenerator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DagTranslator {

  public List<Level> translateFromDag(Dag dag) {
    List<Level> returnList = new ArrayList<>();
    Set<TaskNode> usedTasks = new HashSet<>();
    Set<TaskNode> tasksToProcces = new HashSet<>(dag.getTasks());

    IdGenerator levelsIdGenerator = new IdGenerator();
    while (!tasksToProcces.isEmpty()) {
      usedTasks.clear();
      Level level = new Level().withId(levelsIdGenerator.nextValue());

      for (TaskNode task : tasksToProcces) {
        if (!task.hasParents()) {
          usedTasks.add(task);
        }
      }

      for (TaskNode parent : usedTasks) {
        for (TaskNode child : parent.getChildren()) {
          child.removeParent(parent);
        }
        level.withTask(
            new Task()
                .withName(parent.getName())
                .withLongId(parent.getLongId())
                .withEstimatedResource(CalculationUtils.doubleToLong(parent.getEstimatedCcu())));
      }

      returnList.add(level);
      tasksToProcces.removeAll(usedTasks);

    }

    return returnList;
  }
}
