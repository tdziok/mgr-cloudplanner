package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import java.util.HashMap;
import java.util.Map;

public class LevelEstimatedResultsToDisplay {

  private long levelId;
  private long tasksCount;
  private long averageLevelCCU;
  private long levelTime;
  private long estimatedCost;
  private Map<Long, Long> assignedVms = new HashMap<>();

  public void setLevelId(long levelId) {
    this.levelId = levelId;
  }

  public long getLevelId() {
    return levelId;
  }

  public long getTasksCount() {
    return tasksCount;
  }

  public void setTasksCount(long tasksCount) {
    this.tasksCount = tasksCount;
  }

  public long getAverageLevelCCu() {
    return averageLevelCCU;
  }

  public void setAverageLevelCCU(long averageLevelCCU) {
    this.averageLevelCCU = averageLevelCCU;
  }

  public long getLevelTime() {
    return levelTime;
  }

  public void setLevelTime(long levelTime) {
    this.levelTime = levelTime;
  }

  public long getEstimatedCost() {
    return estimatedCost;
  }

  public void setEstimatedCost(long estimatedCost) {
    this.estimatedCost = estimatedCost;
  }

  public Map<Long, Long> getAssignedVms() {
    return assignedVms;
  }

  public void addVmWithTasksCount(long vmId, long taskCount) {
    assignedVms.put(vmId, taskCount);
  }

}
