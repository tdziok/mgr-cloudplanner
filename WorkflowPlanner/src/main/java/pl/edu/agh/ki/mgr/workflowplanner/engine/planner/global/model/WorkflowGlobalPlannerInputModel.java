package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;

import java.util.List;

public class WorkflowGlobalPlannerInputModel {

  private final Long globalDeadline;
  private final List<Level> levelInfoList;
  private final List<Vm> availableVms;

  public WorkflowGlobalPlannerInputModel(Long globalDeadline, List<Level> levelInfoList, List<Vm> availableVms) {
    super();
    this.globalDeadline = globalDeadline;
    this.levelInfoList = levelInfoList;
    this.availableVms = availableVms;
  }

  public Long getGlobalDeadline() {
    return globalDeadline;
  }

  public List<Level> getLevelInfoList() {
    return levelInfoList;
  }

  public List<Vm> getAvailableVms() {
    return availableVms;
  }

  public int getVmsCount() {
    return availableVms.size();
  }

  public int getLevelsCount() {
    return levelInfoList.size();
  }

}
