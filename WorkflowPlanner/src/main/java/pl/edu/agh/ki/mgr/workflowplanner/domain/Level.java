package pl.edu.agh.ki.mgr.workflowplanner.domain;

import java.util.ArrayList;
import java.util.List;

public class Level implements Comparable<Level> {

  private Long id;
  private List<Task> tasks;

  public Level() {
    tasks = new ArrayList<>();
  }

  public Level(Long id) {
    this();
    this.id = id;
  }

  public Level(Long id, List<Task> tasks) {
    super();
    this.id = id;
    this.tasks = tasks;
  }

  public Level withId(Long id) {
    this.id = id;
    return this;
  }

  public Level withTask(Task task) {
    tasks.add(task);
    return this;
  }

  public Long getId() {
    return id;
  }

  public List<Task> getTasks() {
    return tasks;
  }

  public Long getAverageRequiredCcu() {
    final double averageCcu = getTasks().stream()
        .mapToLong(Task::getEstimatedResource)
        .average()
        .getAsDouble();
    return (long) Math.ceil(averageCcu);
  }

  public int getTaskCount() {
    return tasks.size();
  }

  @Override
  public int compareTo(Level o) {
    long delta = this.getId() - o.getId();
    return (int) delta;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Level other = (Level) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    return true;
  }

}
