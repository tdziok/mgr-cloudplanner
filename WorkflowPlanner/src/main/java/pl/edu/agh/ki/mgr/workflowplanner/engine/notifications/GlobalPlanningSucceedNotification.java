package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.GLOBAL_PLANNING_SUCCEED;

public class GlobalPlanningSucceedNotification extends AbstractLevelAwareNotification {
  public GlobalPlanningSucceedNotification(int level) {
    super(GLOBAL_PLANNING_SUCCEED, level);
  }

}
