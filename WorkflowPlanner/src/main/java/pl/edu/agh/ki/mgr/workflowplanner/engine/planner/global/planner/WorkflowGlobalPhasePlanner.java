package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import jCMPL.*;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplSolutionSaver;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils;
import pl.edu.agh.ki.mgr.workflowplanner.utils.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.cmpl.CmplConstants.*;

abstract class WorkflowGlobalPhasePlanner implements GlobalPhasePlanner {

  private final String pathToModel;
  private final GlobalPlanningModel planningModel;

  protected WorkflowGlobalPhasePlanner(String pathToModel, GlobalPlanningModel planningModel) {
    this.pathToModel = pathToModel;
    this.planningModel = planningModel;
  }

  public WorkflowAssignmentResult planWorkflow(WorkflowGlobalPlannerInputModel inputModel, String pathToSolution) {
    try {
      return plan(inputModel, pathToSolution);
    } catch (CmplException e) {
      if (!e.toString().toLowerCase().contains(("no solution found"))) {
        Logger.LOGGER.error("Error in global planning phase.", e);
      }
      return WorkflowAssignmentResult.failureResult(planningModel, e);
    }
  }

  private WorkflowAssignmentResult plan(WorkflowGlobalPlannerInputModel inputModel, String pathToSolution) throws CmplException {
    final Cmpl cmplModel = new Cmpl(pathToModel);
    final int vmCount = inputModel.getVmsCount();
    final int levelsCount = inputModel.getLevelsCount();

    CmplSet vmsSet = new CmplSet(CMPL_V_VMS);
    vmsSet.setValues(1, vmCount);

    CmplSet levelsSet = new CmplSet(CMPL_L_LEVELS);
    levelsSet.setValues(1, levelsCount);

    CmplParameter tasksCountInCountParam = new CmplParameter(CMPL_L_l_GROUPS_COUNT, levelsSet);
    int[] tasksCountInLevelArray = new int[levelsCount];
    for (int i = 0; i < inputModel.getLevelInfoList().size(); i++) {
      Level levelInfo = inputModel.getLevelInfoList().get(i);
      tasksCountInLevelArray[i] = levelInfo.getTaskCount();
    }
    tasksCountInCountParam.setValues(tasksCountInLevelArray);

    long[][] taskCosts = new long[levelsCount][vmCount];
    long[][] taskTime = new long[levelsCount][vmCount];

    for (int i = 0; i < inputModel.getLevelInfoList().size(); i++) {
      Level level = inputModel.getLevelInfoList().get(i);
      long avgTaskInLevelCcuResources = level.getAverageRequiredCcu();

      for (int j = 0; j < inputModel.getAvailableVms().size(); j++) {
        Vm vm = inputModel.getAvailableVms().get(j);
        long computationTime = CalculationUtils.calculateComputationTime(avgTaskInLevelCcuResources, vm.getCcu());
        long cost = computationTime * vm.getPricePerHour();
        taskTime[i][j] = computationTime;
        taskCosts[i][j] = cost;
      }

    }

    CmplParameter taskFromLevelOnVmCostParam = new CmplParameter(CMPL_C_COST, levelsSet, vmsSet);
    taskFromLevelOnVmCostParam.setValues(taskCosts);

    CmplParameter taskFromLevelOnVmTimeParam = new CmplParameter(CMPL_Ta_TIME, levelsSet, vmsSet);
    taskFromLevelOnVmTimeParam.setValues(taskTime);

    CmplParameter globalDeadlineParam = new CmplParameter(CMPL_D_GLOBAL_DEADLINE);
    globalDeadlineParam.setValues(inputModel.getGlobalDeadline());

    cmplModel.setSets(vmsSet, levelsSet);
    cmplModel.setParameters(globalDeadlineParam, tasksCountInCountParam, taskFromLevelOnVmCostParam, taskFromLevelOnVmTimeParam);

    cmplModel.solve();

    CmplSolutionSaver.save(pathToSolution, cmplModel);

    return prepareResponse(cmplModel, inputModel);
  }

  private WorkflowAssignmentResult prepareResponse(Cmpl cmplModel, WorkflowGlobalPlannerInputModel inputModel) throws NumberFormatException, CmplException {
    WorkflowAssignmentResult response = WorkflowAssignmentResult.successfulResult(planningModel);

    Pattern numberOfTaskOnVmPattern = Pattern.compile(CMPL_Q_NUMBER_OF_TASKS_ON_VM + "\\[(?<idxi>\\d+),(?<idxj>\\d+)\\]");
    Pattern actualTimePattern = Pattern.compile(CMPL_Te_EXECUTION_TIME_FOR_LEVEL + "\\[(?<idx>\\d+)\\]");

    for (CmplSolElement cmplSolElement : cmplModel.solution().variables()) {
      String name = cmplSolElement.name();

      Matcher tMatrixMatcher = numberOfTaskOnVmPattern.matcher(name);
      if (tMatrixMatcher.matches()) {
        int i = Integer.parseInt(tMatrixMatcher.group("idxi")) - 1;
        int j = Integer.parseInt(tMatrixMatcher.group("idxj")) - 1;

        Long activity = (Long) cmplSolElement.activity();
        if (activity.longValue() > 0) {
          Level assignedLevel = inputModel.getLevelInfoList().get(i);
          Vm vm = inputModel.getAvailableVms().get(j);
          response.addAssignment(assignedLevel, vm, activity.longValue());
        }

        continue;
      }

      Matcher timeMatcher = actualTimePattern.matcher(name);
      if (timeMatcher.matches()) {
        int i = Integer.parseInt(timeMatcher.group("idx")) - 1;
        Level assignedLevel = inputModel.getLevelInfoList().get(i);

        Double activity = (Double) cmplSolElement.activity();
        long levelTime = (long) Math.ceil(activity.doubleValue());
        response.withTimeForLevel(assignedLevel, levelTime);
        continue;
      }
    }

    return response;
  }
}
