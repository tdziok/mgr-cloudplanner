package pl.edu.agh.ki.mgr.workflowplanner.domain;

public class Vm implements Comparable<Vm> {

  private Long id;
  private Long ccu;
  private Long pricePerHour;
  private String name;

  public Vm() {
  }

  public Vm(Long id) {
    this.id = id;
  }

  public Vm withId(Long id) {
    this.id = id;
    return this;
  }

  public Vm withCcu(Long ccu) {
    this.ccu = ccu;
    return this;
  }

  public Vm withPricePerHour(Long pricePerHour) {
    this.pricePerHour = pricePerHour;
    return this;
  }

  public Vm withName(String name) {
    this.name = name;
    return this;
  }

  public long getId() {
    return id;
  }

  public long getCcu() {
    return ccu;
  }

  public long getPricePerHour() {
    return pricePerHour;
  }

  public String getName() {
    return name;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Vm other = (Vm) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    return true;
  }

  @Override
  public int compareTo(Vm o) {
    long diff = this.getId() - o.getId();
    return (int) diff;
  }

}
