package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import java.util.ArrayList;
import java.util.List;

public class LevelExecutionResultsToDisplay {

  private Long levelId;
  private Long executionTime;
  private Long executionCost;

  private List<TaskExecutionResultsToDisplay> taskExecutionResultToDisplays;

  public LevelExecutionResultsToDisplay() {
    taskExecutionResultToDisplays = new ArrayList<>();
  }

  public Long getLevelId() {
    return levelId;
  }

  public void setLevelId(Long levelId) {
    this.levelId = levelId;
  }

  public Long getExecutionTime() {
    return executionTime;
  }

  public void setExecutionTime(Long executionTime) {
    this.executionTime = executionTime;
  }

  public Long getExecutionCost() {
    return executionCost;
  }

  public void setExecutionCost(Long executionCost) {
    this.executionCost = executionCost;
  }

  public List<TaskExecutionResultsToDisplay> getTaskExecutionResultToDisplays() {
    return taskExecutionResultToDisplays;
  }

  public void addTaskExecutionResult(TaskExecutionResultsToDisplay taskExecutionResultsToDisplay) {
    this.taskExecutionResultToDisplays.add(taskExecutionResultsToDisplay);
  }

}
