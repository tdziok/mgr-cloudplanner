package pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers;

public class GaussianLongerExecutionTime extends GaussianExecutionTimeModifier {

  @Override
  public double getNextRandom() {
    return super.getNextRandom() * 1.25;
  }

  @Override
  public String getName() {
    return "GaussianLonger";
  }
}
