package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.DataAccumulator;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.utils.CalculationUtils.*;

public class ResultsToDisplayFactory {

  public WorkflowAssignmentResultsToDisplay buildWorkflowAssignmentResults(WorkflowAssignmentResult planResult) {
    WorkflowAssignmentResultsToDisplay results = new WorkflowAssignmentResultsToDisplay();
    final ArrayList<Level> levels = new ArrayList<>(planResult.getLevelToVmsMap().keySet());
    List<LevelEstimatedResultsToDisplay> levelResultsList = levels
        .stream()
        .sorted()
        .map(level -> levelToLevelEstimatedResult(level, planResult))
        .collect(toList());

    results.setLevels(levelResultsList);
    results.setTotalEstimatedCost((long) planResult.getCost());
    long totalEstimatedTime = levelResultsList.stream().map(LevelEstimatedResultsToDisplay::getLevelTime)
        .reduce((t, u) -> t + u)
        .orElse(-1L);
    results.setTotalEstimatedTime(totalEstimatedTime);
    results.setLevels(levelResultsList);
    results.setGlobalPlanningModel(planResult.getPlanningModel());

    return results;
  }

  private LevelEstimatedResultsToDisplay levelToLevelEstimatedResult(Level level, WorkflowAssignmentResult planResult) {
    LevelEstimatedResultsToDisplay levelResults = new LevelEstimatedResultsToDisplay();
    final LongWrapper levelEstimatedCost = new LongWrapper();

    planResult.getLevelToVmsMap()
        .get(level)
        .stream()
        .forEach(vm -> {
          Long assignedTasksNumber = vm.getAssignedTasksNumber();
          levelResults.addVmWithTasksCount(vm.getVm().getId(), assignedTasksNumber);
          final Long averageRequiredCcu = level.getAverageRequiredCcu();
          final long ccu = vm.getVm().getCcu();
          final long vmPricePerHour = vm.getVm().getPricePerHour();
          final long computationTime = calculateComputationTime(averageRequiredCcu, ccu);
          final long computationCost = calculateComputationCost(computationTime, vmPricePerHour);
          levelEstimatedCost.addToValue(assignedTasksNumber * computationCost);
        });

    levelResults.setLevelTime(planResult.getLevelTime(level));
    levelResults.setAverageLevelCCU(level.getAverageRequiredCcu());
    levelResults.setTasksCount(level.getTaskCount());
    levelResults.setLevelId(level.getId());
    levelResults.setEstimatedCost(levelEstimatedCost.getValue());
    return levelResults;
  }

  public LevelAssignmentResultsToDisplay buildLevelAssignmentResults(final LevelAssignmentResult planResult) {
    LevelAssignmentResultsToDisplay levelAssignmentResultsToDisplay = new LevelAssignmentResultsToDisplay();
    levelAssignmentResultsToDisplay.setLevelId(planResult.getLevel().getId());
    List<LevelDetailedResultsToDisplay> levels = new ArrayList<>();

    Map<Long, Long> vmToTimeMap = new HashMap<>();
    planResult.getTaskToVm()
        .keySet()
        .stream()
        .sorted()
        .forEach(task -> {
          LevelDetailedResultsToDisplay levelDetailedResultsToDisplay = new LevelDetailedResultsToDisplay();
          Vm vm = planResult.getTaskToVm().get(task);
          long time = calculateComputationTime(task.getEstimatedResource(), vm.getCcu());
          long cost = time * vm.getPricePerHour();

          levelDetailedResultsToDisplay.setEstimatedCost(cost);
          levelDetailedResultsToDisplay.setEstimatedTime(time);
          levelDetailedResultsToDisplay.setTaskCcu(task.getEstimatedResource());
          levelDetailedResultsToDisplay.setTaskId(task.getId());
          levelDetailedResultsToDisplay.setVmId(vm.getId());
          levelDetailedResultsToDisplay.setVmCcu(vm.getCcu());

          Long vmTime = vmToTimeMap.get(vm.getId());
          if (vmTime == null) {
            vmToTimeMap.put(vm.getId(), time);
          } else {
            vmToTimeMap.put(vm.getId(), time + vmTime);
          }

          levels.add(levelDetailedResultsToDisplay);
        });

    long estimatedTime = vmToTimeMap.entrySet()
        .stream()
        .map(Map.Entry::getValue)
        .max(Comparator.comparing(Long::intValue))
        .get();

    final long levelCostSum = levels.stream()
        .mapToLong(LevelDetailedResultsToDisplay::getEstimatedCost)
        .sum();
    long estimatedCost = doubleToLong(levelCostSum);

    levelAssignmentResultsToDisplay.setEstimatedCost(estimatedCost);
    levelAssignmentResultsToDisplay.setEstimatedTime(estimatedTime);
    levelAssignmentResultsToDisplay.setLevels(levels);

    return levelAssignmentResultsToDisplay;
  }

  public LevelExecutionResultsToDisplay buildLevelExecutionResults(ExecutionResults results) {
    LevelExecutionResultsToDisplay levelExecutionResultsToDisplay = new LevelExecutionResultsToDisplay();
    levelExecutionResultsToDisplay.setLevelId(results.getLevelId());
    levelExecutionResultsToDisplay.setExecutionTime(results.getRealExecutionTime());

    final DataAccumulator totalCost = new DataAccumulator();
    results.getTaskTimeMap().keySet()
        .stream()
        .sorted()
        .forEach(taskId -> {
          TaskExecutionResultsToDisplay singleTaskExecutionResult = new TaskExecutionResultsToDisplay();
          long taskTime = results.getTaskTimeMap().get(taskId).longValue();
          Vm vm = results.getTaskToVmMap().get(taskId);

          long taskComputationCost = calculateComputationCost(taskTime, vm.getPricePerHour());
          singleTaskExecutionResult.setExecutionCost(taskComputationCost);
          singleTaskExecutionResult.setExecutionTime(taskTime);
          singleTaskExecutionResult.setTaskCcu(taskId.getEstimatedResource());
          singleTaskExecutionResult.setTaskId(taskId.getId());
          singleTaskExecutionResult.setVmId(vm.getId());

          levelExecutionResultsToDisplay.addTaskExecutionResult(singleTaskExecutionResult);
          totalCost.add(taskComputationCost);

        });

    levelExecutionResultsToDisplay.setExecutionCost(totalCost.getCurrentValue());
    return levelExecutionResultsToDisplay;
  }

  private static class LongWrapper {
    private long value;

    LongWrapper() {
      value = 0L;
    }

    long getValue() {
      return value;
    }

    void addToValue(long value) {
      this.value += value;
    }

  }
}
