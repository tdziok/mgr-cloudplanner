package pl.edu.agh.ki.mgr.workflowplanner.engine.cloudmanager;

import pl.edu.agh.ki.mgr.workflowplanner.engine.executor.timemodifiers.ExecutionTimeModifier;

public class CloudManagerParameters {

  private final ExecutionTimeModifier executionTimeModifier;

  private final boolean shouldTakeIntoAccountCurrentTime;

  private final String resultsRootDirectory;

  public CloudManagerParameters(ExecutionTimeModifier executionTimeModifier, boolean shouldTakeIntoAccountCurrentTime, String resultsRootDirectory) {
    this.executionTimeModifier = executionTimeModifier;
    this.shouldTakeIntoAccountCurrentTime = shouldTakeIntoAccountCurrentTime;
    this.resultsRootDirectory = resultsRootDirectory;
  }

  public ExecutionTimeModifier getExecutionTimeModifier() {
    return executionTimeModifier;
  }

  public boolean shouldTakeIntoAccountRealExecutionTime() {
    return shouldTakeIntoAccountCurrentTime;
  }

  public String getResultsRootDirectory() {
    return resultsRootDirectory;
  }

}
