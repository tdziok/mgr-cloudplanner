package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.notifications.NotificationType.ITERATION_STARTED;

public class IterationStartedNotification extends AbstractLevelAwareNotification {

  public IterationStartedNotification(int level) {
    super(ITERATION_STARTED, level);
  }
}
