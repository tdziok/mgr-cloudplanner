package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class ExecutingTasksStartedNotification extends AbstractLevelAwareNotification {
  public ExecutingTasksStartedNotification(int level) {
    super(NotificationType.EXECUTING_TASKS_STARTED, level);
  }
}
