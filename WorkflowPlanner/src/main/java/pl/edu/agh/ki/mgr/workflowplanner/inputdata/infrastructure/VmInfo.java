package pl.edu.agh.ki.mgr.workflowplanner.inputdata.infrastructure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VmInfo {

  @Expose
  private String NAME;
  @Expose
  private String ISCOMPUTEOPTIMIZED;
  @Expose
  private long CPU;
  @Expose
  private String ECU;
  @Expose
  private double MEMORY;
  @SerializedName("INSTANCE STORAGE")
  @Expose
  private String INSTANCESTORAGE;
  @Expose
  private long SIZE;
  @Expose
  private String ISSSD;
  @SerializedName("LINUX/UNIX USAGE")
  @Expose
  private String LINUXUNIXUSAGE;
  @Expose
  private long CENTSPERHOUR;
  @SerializedName("CCU VALUE")
  @Expose
  private double CCUVALUE;
  @Expose
  private long CCU;
  @Expose
  private long AVAILABLEINSTANCES;

  /**
   * @return The NAME
   */
  public String getNAME() {
    return NAME;
  }

  /**
   * @param NAME The NAME
   */
  public void setNAME(String NAME) {
    this.NAME = NAME;
  }

  /**
   * @return The ISCOMPUTEOPTIMIZED
   */
  public String getISCOMPUTEOPTIMIZED() {
    return ISCOMPUTEOPTIMIZED;
  }

  /**
   * @param ISCOMPUTEOPTIMIZED The ISCOMPUTEOPTIMIZED
   */
  public void setISCOMPUTEOPTIMIZED(String ISCOMPUTEOPTIMIZED) {
    this.ISCOMPUTEOPTIMIZED = ISCOMPUTEOPTIMIZED;
  }

  /**
   * @return The CPU
   */
  public long getCPU() {
    return CPU;
  }

  /**
   * @param CPU The CPU
   */
  public void setCPU(long CPU) {
    this.CPU = CPU;
  }

  /**
   * @return The ECU
   */
  public String getECU() {
    return ECU;
  }

  /**
   * @param ECU The ECU
   */
  public void setECU(String ECU) {
    this.ECU = ECU;
  }

  /**
   * @return The MEMORY
   */
  public double getMEMORY() {
    return MEMORY;
  }

  /**
   * @param MEMORY The MEMORY
   */
  public void setMEMORY(long MEMORY) {
    this.MEMORY = MEMORY;
  }

  /**
   * @return The INSTANCESTORAGE
   */
  public String getINSTANCESTORAGE() {
    return INSTANCESTORAGE;
  }

  /**
   * @param INSTANCESTORAGE The INSTANCE STORAGE
   */
  public void setINSTANCESTORAGE(String INSTANCESTORAGE) {
    this.INSTANCESTORAGE = INSTANCESTORAGE;
  }

  /**
   * @return The SIZE
   */
  public long getSIZE() {
    return SIZE;
  }

  /**
   * @param SIZE The SIZE
   */
  public void setSIZE(long SIZE) {
    this.SIZE = SIZE;
  }

  /**
   * @return The ISSSD
   */
  public String getISSSD() {
    return ISSSD;
  }

  /**
   * @param ISSSD The ISSSD
   */
  public void setISSSD(String ISSSD) {
    this.ISSSD = ISSSD;
  }

  /**
   * @return The LINUXUNIXUSAGE
   */
  public String getLINUXUNIXUSAGE() {
    return LINUXUNIXUSAGE;
  }

  /**
   * @param LINUXUNIXUSAGE The LINUX/UNIX USAGE
   */
  public void setLINUXUNIXUSAGE(String LINUXUNIXUSAGE) {
    this.LINUXUNIXUSAGE = LINUXUNIXUSAGE;
  }

  /**
   * @return The CENTSPERHOUR
   */
  public long getCENTSPERHOUR() {
    return CENTSPERHOUR;
  }

  /**
   * @param CENTSPERHOUR The CENTSPERHOUR
   */
  public void setCENTSPERHOUR(long CENTSPERHOUR) {
    this.CENTSPERHOUR = CENTSPERHOUR;
  }

  /**
   * @return The CCUVALUE
   */
  public double getCCUVALUE() {
    return CCUVALUE;
  }

  /**
   * @param CCUVALUE The CCU VALUE
   */
  public void setCCUVALUE(double CCUVALUE) {
    this.CCUVALUE = CCUVALUE;
  }

  /**
   * @return The CCU
   */
  public long getCCU() {
    return CCU;
  }

  /**
   * @param CCU The CCU
   */
  public void setCCU(long CCU) {
    this.CCU = CCU;
  }

  /**
   * @return The AVAILABLEINSTANCES
   */
  public long getAVAILABLEINSTANCES() {
    return AVAILABLEINSTANCES;
  }

  /**
   * @param AVAILABLEINSTANCES The AVAILABLEINSTANCES
   */
  public void setAVAILABLEINSTANCES(long AVAILABLEINSTANCES) {
    this.AVAILABLEINSTANCES = AVAILABLEINSTANCES;
  }
}
