package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public interface PlotGenerationObserver {

  void onUpdate(SimulationProgressNotification notification);

}
