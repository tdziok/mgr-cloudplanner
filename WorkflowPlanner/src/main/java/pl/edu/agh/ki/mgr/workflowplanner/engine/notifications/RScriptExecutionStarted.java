package pl.edu.agh.ki.mgr.workflowplanner.engine.notifications;

public class RScriptExecutionStarted extends AbstractSimulationNotification {
  private final String commandLine;

  public RScriptExecutionStarted(String commandLine) {
    super(NotificationType.RSCRIPT_EXECUTION_STARTED);
    this.commandLine = commandLine;
  }

  @Override
  public String getDescription() {
    return super.getDescription() + ". Command: " + commandLine;
  }
}
