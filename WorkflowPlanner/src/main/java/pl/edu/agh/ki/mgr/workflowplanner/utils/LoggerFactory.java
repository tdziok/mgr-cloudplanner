package pl.edu.agh.ki.mgr.workflowplanner.utils;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;

public class LoggerFactory {

  public static final String FILE_LOGGER = "FileLogger";

  public static FileAppender fileAppender(String loggerPath) {
    FileAppender fileAppender = new FileAppender();
    fileAppender.setName(FILE_LOGGER);
    fileAppender.setFile(loggerPath);
    fileAppender.setLayout(new PatternLayout("%d %m%n"));
    fileAppender.setThreshold(Level.DEBUG);
    fileAppender.setAppend(true);
    fileAppender.activateOptions();
    return fileAppender;
  }

}
