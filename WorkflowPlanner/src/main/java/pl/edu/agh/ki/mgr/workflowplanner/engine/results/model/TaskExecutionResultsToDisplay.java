package pl.edu.agh.ki.mgr.workflowplanner.engine.results.model;

public class TaskExecutionResultsToDisplay {

  private long taskId;
  private long taskCcu;
  private long vmId;
  private long executionTime;
  private long executionCost;

  public long getTaskId() {
    return taskId;
  }

  public void setTaskId(long taskId) {
    this.taskId = taskId;
  }

  public long getTaskCcu() {
    return taskCcu;
  }

  public void setTaskCcu(long taskCcu) {
    this.taskCcu = taskCcu;
  }

  public long getVmId() {
    return vmId;
  }

  public void setVmId(long vmId) {
    this.vmId = vmId;
  }

  public long getExecutionTime() {
    return executionTime;
  }

  public void setExecutionTime(long executionTime) {
    this.executionTime = executionTime;
  }

  public long getExecutionCost() {
    return executionCost;
  }

  public void setExecutionCost(long executionCost) {
    this.executionCost = executionCost;
  }

}
