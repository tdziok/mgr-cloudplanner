package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.utils.IdGenerator;

import java.util.List;

import static java.util.Arrays.asList;

abstract class AbstractStd_4_2_2_1_Input extends AbstractGlobalInput {

  protected Vm vm1 = new Vm(1L)
      .withCcu(1L)
      .withPricePerHour(1L);
  protected Vm vm2 = new Vm(2L)
      .withCcu(2L)
      .withPricePerHour(4L);
  protected Vm vm3 = new Vm(3L)
      .withCcu(4L)
      .withPricePerHour(6L);

  private IdGenerator taskIdGen = new IdGenerator();

  protected Level level1 = new Level(1L)
      .withTask(new Task(taskIdGen.nextValue())
          .withEstimatedResource(1L))
      .withTask(new Task(taskIdGen.nextValue())
          .withEstimatedResource(1L))
      .withTask(new Task(taskIdGen.nextValue())
          .withEstimatedResource(2L))
      .withTask(new Task(taskIdGen.nextValue())
          .withEstimatedResource(4L));

  protected Level level2 = new Level(2L)
      .withTask(new Task(taskIdGen.nextValue())
              .withEstimatedResource(2L)
      ).withTask(new Task(taskIdGen.nextValue())
              .withEstimatedResource(2L)
      );
  protected Level level3 = new Level(3L)
      .withTask(new Task(taskIdGen.nextValue())
              .withEstimatedResource(1L)
      ).withTask(new Task(taskIdGen.nextValue())
              .withEstimatedResource(4L)
      );
  protected Level level4 = new Level(4L)
      .withTask(new Task(taskIdGen.nextValue())
              .withEstimatedResource(6L)
      );

  protected WorkflowGlobalPlannerInputModel inputModel(Long deadline) {
    List<Level> levels = asList(level1, level2, level3, level4);
    List<Vm> vms = asList(vm1, vm2, vm3);
    return new WorkflowGlobalPlannerInputModel(deadline, levels, vms);
  }

}
