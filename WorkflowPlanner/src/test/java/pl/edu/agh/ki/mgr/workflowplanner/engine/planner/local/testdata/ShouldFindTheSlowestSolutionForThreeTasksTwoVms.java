package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;

import java.util.HashMap;
import java.util.Map;

public class ShouldFindTheSlowestSolutionForThreeTasksTwoVms extends AbstractLevelInput {

  public ShouldFindTheSlowestSolutionForThreeTasksTwoVms() {
    Vm vm1 = new Vm(1L)
        .withCcu(1L)
        .withPricePerHour(4L);
    Vm vm2 = new Vm(2L)
        .withCcu(4L)
        .withPricePerHour(4L);

    Task task1 = new Task(1L)
        .withEstimatedResource(4L);
    Task task2 = new Task(2L)
        .withEstimatedResource(4L);
    Task task3 = new Task(3L)
        .withEstimatedResource(4L);

    Level level = new Level(1L)
        .withTask(task1)
        .withTask(task2)
        .withTask(task3);

    Map<Vm, Long> vms = new HashMap<>();
    vms.put(vm1, 3L);
    vms.put(vm2, 0L);
    inputModel = new LocalPlannerInputModel(level, vms);

    expectedPlan = LevelAssignmentResult.succeedResult(level);
    expectedPlan.setAssignment(task1, vm1);
    expectedPlan.setAssignment(task2, vm1);
    expectedPlan.setAssignment(task3, vm1);

  }
}
