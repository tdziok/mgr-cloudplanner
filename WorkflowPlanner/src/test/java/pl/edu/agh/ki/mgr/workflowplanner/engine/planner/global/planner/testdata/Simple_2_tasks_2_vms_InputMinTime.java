package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;

import java.util.List;

import static java.util.Arrays.asList;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel.ALTERNATIVE_MINIMIZE_TIME;

public class Simple_2_tasks_2_vms_InputMinTime extends AbstractGlobalInput {

  public Simple_2_tasks_2_vms_InputMinTime() {

    Level level1 = new Level()
        .withId(1L)
        .withTask(new Task()
            .withLongId(1L)
            .withEstimatedResource(4L))
        .withTask(new Task()
            .withLongId(2L)
            .withEstimatedResource(8L));

    Vm vm1 = new Vm()
        .withId(1L)
        .withCcu(2L)
        .withPricePerHour(1L);
    Vm vm2 = new Vm()
        .withId(2L)
        .withCcu(8L)
        .withPricePerHour(1L);

    Long deadline = 1L;
    List<Level> levels = asList(level1);
    List<Vm> vms = asList(vm1, vm2);

    input = new WorkflowGlobalPlannerInputModel(deadline, levels, vms);

    result = WorkflowAssignmentResult.successfulResult(ALTERNATIVE_MINIMIZE_TIME)
        .addAssignment(level1, vm2, 2L)
        .withTimeForLevel(level1, 2L);

    cost = 2.00;

  }
}
