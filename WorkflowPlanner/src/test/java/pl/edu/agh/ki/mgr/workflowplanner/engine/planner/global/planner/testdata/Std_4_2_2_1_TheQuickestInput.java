package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel.MAIN_MINIMIZE_COST_UNDER_DEADLINE;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult.successfulResult;

public class Std_4_2_2_1_TheQuickestInput extends AbstractStd_4_2_2_1_Input {

  public Std_4_2_2_1_TheQuickestInput() {
    Long deadline = 7L;
    input = inputModel(deadline);

    result = successfulResult(MAIN_MINIMIZE_COST_UNDER_DEADLINE)
        .addAssignment(level1, vm1, 1L) // 2
        .addAssignment(level1, vm2, 2L) // 8
        .addAssignment(level1, vm3, 1L) // 6
        .withTimeForLevel(level1, 2L)

        .addAssignment(level2, vm2, 1L) // 4
        .addAssignment(level2, vm3, 1L) // 6
        .withTimeForLevel(level2, 1L)

        .addAssignment(level3, vm3, 2L) // 12
        .withTimeForLevel(level3, 2L)

        .addAssignment(level4, vm3, 1L) //12
        .withTimeForLevel(level4, 2L);

    cost = 50.00;
  }
}
