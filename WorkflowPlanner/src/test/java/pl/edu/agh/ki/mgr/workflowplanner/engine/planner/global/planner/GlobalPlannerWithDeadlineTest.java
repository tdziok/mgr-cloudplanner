package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult.VmWrapper;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class GlobalPlannerWithDeadlineTest {

  private final WorkflowGlobalPlannerInputModel inputModel;
  private final WorkflowAssignmentResult expectedPlan;
  private final double expectedCost;
  private GlobalPlannerWithDeadline globalPlannerWithDeadline = new GlobalPlannerWithDeadline();

  public GlobalPlannerWithDeadlineTest(AbstractGlobalInput testInput) {
    this.inputModel = testInput.getInput();
    this.expectedPlan = testInput.getResult();
    this.expectedCost = testInput.getExpectedCost();
  }

  @Parameterized.Parameters
  public static Collection parameters() {
    return Arrays.asList(new Object[][]{
        {new Simple_1_1_Input()},
        {new Simple_2_tasks_2_vms_Input()},
        {new Std_4_2_2_1_TheQuickestInput()},
        {new Std_4_2_2_1_TheCheapest()}
    });
  }

  @Test
  public void shouldPlanWorkflow() {
    // when

    final WorkflowAssignmentResult actualPlan = globalPlannerWithDeadline.planWorkflow(inputModel, null);

    // then
    assertEquals(expectedCost, actualPlan.getCost(), 0.01);

    for (Level givenLevel : inputModel.getLevelInfoList()) {
      Long expectedLayerTime = expectedPlan.getLevelTime(givenLevel);
      Long actualLayerTime = actualPlan.getLevelTime(givenLevel);
      assertEquals("Comparing computation time for layer.", expectedLayerTime,
          actualLayerTime);

      List<VmWrapper> expectedVms = expectedPlan.getLevelToVmsMap().get(
          givenLevel);
      List<VmWrapper> actualVms = actualPlan.getLevelToVmsMap().get(givenLevel);

      List<Long> expectedVmIds = extractVmIds(expectedVms);
      List<Long> actualVmIds = extractVmIds(actualVms);

      assertArrayEquals("Comparing assigned vms to layer.",
          expectedVmIds.toArray(), actualVmIds.toArray());

      List<Long> expectedTaskCountOnVm = extractVmTaskCount(expectedVms);
      List<Long> actualTaskCountOnVm = extractVmTaskCount(actualVms);

      assertArrayEquals("Comparing assigned task count to VM in layer.",
          expectedTaskCountOnVm.toArray(), actualTaskCountOnVm.toArray());

    }

  }

  private List<Long> extractVmIds(List<VmWrapper> vms) {
    return vms.stream()
        .map(VmWrapper::getVm)
        .map(Vm::getId)
        .sorted()
        .collect(toList());
  }

  private List<Long> extractVmTaskCount(List<VmWrapper> vms) {
    return vms.stream()
        .map(VmWrapper::getAssignedTasksNumber)
        .sorted()
        .collect(toList());
  }
}