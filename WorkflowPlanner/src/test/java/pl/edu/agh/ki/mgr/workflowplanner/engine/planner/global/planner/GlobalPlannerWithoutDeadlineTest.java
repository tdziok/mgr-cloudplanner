package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata.AbstractGlobalInput;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata.Simple_2_tasks_2_vms_InputMinTime;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class GlobalPlannerWithoutDeadlineTest {

  private final WorkflowGlobalPlannerInputModel inputModel;
  private final WorkflowAssignmentResult expectedPlan;
  private final double expectedCost;
  private GlobalPlannerWithoutDeadline globalPlannerWithoutDeadline = new GlobalPlannerWithoutDeadline();

  public GlobalPlannerWithoutDeadlineTest(AbstractGlobalInput testInput) {
    this.inputModel = testInput.getInput();
    this.expectedPlan = testInput.getResult();
    this.expectedCost = testInput.getExpectedCost();
  }

  @Parameterized.Parameters
  public static Collection parameters() {
    return Arrays.asList(new Object[][]{
        {new Simple_2_tasks_2_vms_InputMinTime()}
    });
  }

  @Test
  public void shouldPlanWorkflow() {
    // when

    final WorkflowAssignmentResult actualPlan = globalPlannerWithoutDeadline.planWorkflow(inputModel, null);

    // then
    assertEquals(expectedCost, actualPlan.getCost(), 0.01);

    for (Level givenLevel : inputModel.getLevelInfoList()) {
      Long expectedLayerTime = expectedPlan.getLevelTime(givenLevel);
      Long actualLayerTime = actualPlan.getLevelTime(givenLevel);
      assertEquals("Comparing computation time for layer.", expectedLayerTime,
          actualLayerTime);

      List<WorkflowAssignmentResult.VmWrapper> expectedVms = expectedPlan.getLevelToVmsMap().get(
          givenLevel);
      List<WorkflowAssignmentResult.VmWrapper> actualVms = actualPlan.getLevelToVmsMap().get(givenLevel);

      List<Long> expectedVmIds = extractVmIds(expectedVms);
      List<Long> actualVmIds = extractVmIds(actualVms);

      assertArrayEquals("Comparing assigned vms to layer.",
          expectedVmIds.toArray(), actualVmIds.toArray());

      List<Long> expectedTaskCountOnVm = extractVmTaskCount(expectedVms);
      List<Long> actualTaskCountOnVm = extractVmTaskCount(actualVms);

      assertArrayEquals("Comparing assigned task count to VM in layer.",
          expectedTaskCountOnVm.toArray(), actualTaskCountOnVm.toArray());

    }

  }

  private List<Long> extractVmIds(List<WorkflowAssignmentResult.VmWrapper> vms) {
    return vms.stream()
        .map(WorkflowAssignmentResult.VmWrapper::getVm)
        .map(Vm::getId)
        .sorted()
        .collect(toList());
  }

  private List<Long> extractVmTaskCount(List<WorkflowAssignmentResult.VmWrapper> vms) {
    return vms.stream()
        .map(WorkflowAssignmentResult.VmWrapper::getAssignedTasksNumber)
        .sorted()
        .collect(toList());
  }

}
