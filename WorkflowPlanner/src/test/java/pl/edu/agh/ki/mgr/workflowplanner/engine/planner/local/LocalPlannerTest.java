package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.testdata.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class LocalPlannerTest {

  private final LocalPlannerInputModel plannerInput;
  private final LevelAssignmentResult expectedPlan;

  private final LocalPlanner localPlanner = new LocalPlanner();

  public LocalPlannerTest(AbstractLevelInput testInput) {
    plannerInput = testInput.getInputModel();
    expectedPlan = testInput.getExpectedPlan();
  }

  @Parameterized.Parameters
  public static Collection testInputs() {
    return Arrays.asList(new Object[][]{
        {new OneLevelOneTask()},
        {new ShouldKeepTaskNumOnLayerConstraintInput()},
        {new SimpleLayer()},
        {new ShouldFindTheSlowestSolutionForThreeTasksTwoVms()}
    });
  }

  @Test
  public void shouldPlanOneLevel() {
    // given
    // when
    final LevelAssignmentResult actualPlan = localPlanner.planLevel(plannerInput, null);

    // then
    assertEquals(expectedPlan.getTaskToVm().size(), actualPlan.getTaskToVm().size());

    for (Map.Entry<Task, Vm> entry : expectedPlan.getTaskToVm().entrySet()) {
      final Task task = entry.getKey();
      Vm actualVm = actualPlan.getTaskToVm().get(task);

      final Vm expectedVm = entry.getValue();
      assertEquals(expectedVm.getId(), actualVm.getId());
    }

  }

}