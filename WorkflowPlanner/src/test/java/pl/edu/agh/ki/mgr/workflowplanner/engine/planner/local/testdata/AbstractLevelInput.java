package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LevelAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.local.model.LocalPlannerInputModel;

public abstract class AbstractLevelInput {

  protected LocalPlannerInputModel inputModel;
  protected LevelAssignmentResult expectedPlan;

  public LocalPlannerInputModel getInputModel() {
    return inputModel;
  }

  public LevelAssignmentResult getExpectedPlan() {
    return expectedPlan;
  }
}
