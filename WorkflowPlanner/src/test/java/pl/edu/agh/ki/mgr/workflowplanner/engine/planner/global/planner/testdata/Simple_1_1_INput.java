package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;

import java.util.List;

import static java.util.Arrays.asList;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel.MAIN_MINIMIZE_COST_UNDER_DEADLINE;

public class Simple_1_1_Input extends AbstractGlobalInput {

  public Simple_1_1_Input() {

    Level level1 = new Level()
        .withId(1L)
        .withTask(new Task()
                .withLongId(1L)
                .withEstimatedResource(4L)
        );

    Vm vm1 = new Vm()
        .withId(1L)
        .withCcu(4L)
        .withPricePerHour(4L);

    Long deadline = 6L;
    List<Level> levels = asList(level1);
    List<Vm> vms = asList(vm1);

    input = new WorkflowGlobalPlannerInputModel(deadline, levels, vms);

    result = WorkflowAssignmentResult.successfulResult(MAIN_MINIMIZE_COST_UNDER_DEADLINE)
        .addAssignment(level1, vm1, 1L)
        .withTimeForLevel(level1, 1L);

    cost = 4.00;
  }
}
