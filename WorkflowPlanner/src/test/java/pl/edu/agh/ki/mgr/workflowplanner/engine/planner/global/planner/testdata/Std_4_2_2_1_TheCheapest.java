package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel.MAIN_MINIMIZE_COST_UNDER_DEADLINE;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult.successfulResult;

public class Std_4_2_2_1_TheCheapest extends AbstractStd_4_2_2_1_Input {

  public Std_4_2_2_1_TheCheapest() {
    Long deadline = 24L;
    input = inputModel(deadline);

    result = successfulResult(MAIN_MINIMIZE_COST_UNDER_DEADLINE)
        .addAssignment(level1, vm1, 4L)
        .withTimeForLevel(level1, 8L)

        .addAssignment(level2, vm1, 2L)
        .withTimeForLevel(level2, 4L)

        .addAssignment(level3, vm1, 2L)
        .withTimeForLevel(level3, 6L)

        .addAssignment(level4, vm1, 1L)
        .withTimeForLevel(level4, 6L);

    cost = 24.00;
  }
}
