package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;

public class AbstractGlobalInput {

  protected WorkflowGlobalPlannerInputModel input;
  protected WorkflowAssignmentResult result;
  protected double cost;

  public WorkflowAssignmentResult getResult() {
    return result;
  }

  public WorkflowGlobalPlannerInputModel getInput() {
    return input;
  }

  public double getExpectedCost(){
    return cost;
  }
}
