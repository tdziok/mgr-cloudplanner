package pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.planner.testdata;

import pl.edu.agh.ki.mgr.workflowplanner.domain.Level;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Task;
import pl.edu.agh.ki.mgr.workflowplanner.domain.Vm;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowAssignmentResult;
import pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.WorkflowGlobalPlannerInputModel;

import java.util.List;

import static java.util.Arrays.asList;
import static pl.edu.agh.ki.mgr.workflowplanner.engine.planner.global.model.GlobalPlanningModel.MAIN_MINIMIZE_COST_UNDER_DEADLINE;

public class Simple_2_tasks_2_vms_Input extends AbstractGlobalInput {

  public Simple_2_tasks_2_vms_Input() {

    Level level1 = new Level()
        .withId(1L)
        .withTask(new Task()
            .withLongId(1L)
            .withEstimatedResource(2L))
        .withTask(new Task()
            .withLongId(2L)
            .withEstimatedResource(2L));

    Vm vm1 = new Vm()
        .withId(1L)
        .withCcu(2L)
        .withPricePerHour(4L);
    Vm vm2 = new Vm()
        .withId(2L)
        .withCcu(2L)
        .withPricePerHour(8L);

    Long deadline = 1L;
    List<Level> levels = asList(level1);
    List<Vm> vms = asList(vm1, vm2);

    input = new WorkflowGlobalPlannerInputModel(deadline, levels, vms);

    result = WorkflowAssignmentResult.successfulResult(MAIN_MINIMIZE_COST_UNDER_DEADLINE)
        .addAssignment(level1, vm1, 1L)
        .addAssignment(level1, vm2, 1L)
        .withTimeForLevel(level1, 1L);

    cost = 12.00;
  }
}
