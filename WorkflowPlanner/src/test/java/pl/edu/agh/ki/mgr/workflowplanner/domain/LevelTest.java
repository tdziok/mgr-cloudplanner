package pl.edu.agh.ki.mgr.workflowplanner.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LevelTest {

  @Test
  public void givenOneTask_shouldCalculateAverageTaskCcu() {
    // given
    Level level = new Level()
        .withId(1L)
        .withTask(new Task()
            .withEstimatedResource(1L));

    // when
    final Long averageRequiredCcu = level.getAverageRequiredCcu();

    // then
    assertEquals(1L, averageRequiredCcu.longValue());
  }

  @Test
  public void givenTwoTasks_shouldCalculateAverageTaskWithRoundingUp() {
    // given
    Level level = new Level()
        .withId(1L)
        .withTask(new Task()
            .withEstimatedResource(1L))
        .withTask(new Task()
            .withEstimatedResource(4L));

    // when
    final Long averageRequiredCcu = level.getAverageRequiredCcu();

    // then
    assertEquals(3L, averageRequiredCcu.longValue());
  }

}